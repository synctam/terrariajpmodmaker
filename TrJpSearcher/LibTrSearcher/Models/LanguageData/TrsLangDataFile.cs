namespace LibTrSearcher.Models.LanguageData
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// 言語ファイル
    /// </summary>
    public class TrsLangDataFile
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="fileID">FileID</param>
        public TrsLangDataFile(string fileID)
        {
            this.FileID = fileID;
        }

        /// <summary>
        /// 言語グループの辞書
        /// キーは group
        /// </summary>
        public Dictionary<string, TrsLangDataGroup> Items { get; } =
            new Dictionary<string, TrsLangDataGroup>(StringComparer.CurrentCultureIgnoreCase);

        /// <summary>
        /// FileID
        /// </summary>
        public string FileID { get; }

        /// <summary>
        /// 言語エントリーを追加する
        /// </summary>
        /// <param name="group">グループ</param>
        /// <param name="key">キー</param>
        /// <param name="originalText">原文</param>
        /// <param name="translatedText">翻訳文</param>
        public void AddEntry(
            string group,
            string key,
            string originalText,
            string translatedText)
        {
            if (this.Items.ContainsKey(group))
            {
                var currentGroups = this.Items[group];
                currentGroups.AddEntry(key, originalText, translatedText);
            }
            else
            {
                var newGroup = new TrsLangDataGroup(group);
                this.Items.Add(newGroup.Group, newGroup);

                newGroup.AddEntry(key, originalText, translatedText);
            }
        }

        /// <summary>
        /// 検索テキストで検索し、結果を検索結果のリストに追加する。
        /// </summary>
        /// <param name="results">検索結果のリスト</param>
        /// <param name="text">検索テキスト</param>
        public void Search(TrsLangDataInfo results, string text)
        {
            foreach (var langGroup in this.Items.Values)
            {
                langGroup.Search(results, text, this.FileID);
            }
        }
    }
}

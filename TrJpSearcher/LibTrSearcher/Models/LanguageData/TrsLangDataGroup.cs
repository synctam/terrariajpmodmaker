namespace LibTrSearcher.Models.LanguageData
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// 言語グループ
    /// </summary>
    public class TrsLangDataGroup
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="group">グループ</param>
        public TrsLangDataGroup(string group)
        {
            this.Group = group;
        }

        /// <summary>
        /// 翻訳シートエントリーの辞書
        /// キーは Key
        /// </summary>
        public Dictionary<string, TrsLangDataEntry> Items { get; } =
            new Dictionary<string, TrsLangDataEntry>(StringComparer.CurrentCultureIgnoreCase);

        /// <summary>
        /// グループ
        /// </summary>
        public string Group { get; }

        /// <summary>
        /// 言語エントリーを追加する。
        /// </summary>
        /// <param name="key">キー</param>
        /// <param name="originalText">原文</param>
        /// <param name="translatedText">翻訳文</param>
        public void AddEntry(string key, string originalText, string translatedText)
        {
            if (this.Items.ContainsKey(key))
            {
                ////throw new Exception($"Duplicate key({key})");
            }
            else
            {
                var entry = new TrsLangDataEntry(key, originalText, translatedText);
                this.Items.Add(entry.Key, entry);
            }
        }

        /// <summary>
        /// 検索テキストで検索し、結果を検索結果のリストに追加する。
        /// </summary>
        /// <param name="results">検索結果のリスト</param>
        /// <param name="text">検索テキスト</param>
        /// <param name="fileID">FileID</param>
        public void Search(TrsLangDataInfo results, string text, string fileID)
        {
            this.SearchOriginalText(results, text, fileID);
            this.SearchTranslatedText(results, text, fileID);
        }

        /// <summary>
        /// 検索テキストを検索し検索結果のリストを返す。
        /// </summary>
        /// <param name="results">検索結果のリスト</param>
        /// <param name="searchText">検索テキスト</param>
        /// <param name="fileID">FileID</param>
        public void SearchOriginalText(
            TrsLangDataInfo results, string searchText, string fileID)
        {
            foreach (var entry in this.Items.Values)
            {
                if (entry.OriginalText.Contains(searchText, StringComparison.OrdinalIgnoreCase))
                {
                    if (this.IsValidate(entry.OriginalText, entry.TranslatedText))
                    {
                        results.AddEntry(fileID, this.Group, entry.Key, entry.OriginalText, entry.TranslatedText);
                    }
                }
            }
        }

        /// <summary>
        /// 検索テキストを検索し検索結果のリストを返す。
        /// </summary>
        /// <param name="results">検索結果のリスト</param>
        /// <param name="searchText">検索テキスト</param>
        /// <param name="fileID">FileID</param>
        public void SearchTranslatedText(
            TrsLangDataInfo results, string searchText, string fileID)
        {
            foreach (var entry in this.Items.Values)
            {
                if (entry.TranslatedText.Contains(searchText, StringComparison.OrdinalIgnoreCase))
                {
                    if (this.IsValidate(entry.OriginalText, entry.TranslatedText))
                    {
                        results.AddEntry(fileID, this.Group, entry.Key, entry.OriginalText, entry.TranslatedText);
                    }
                }
            }
        }

        /// <summary>
        /// 検索対象の可否を返す
        /// </summary>
        /// <param name="originalText">原文</param>
        /// <param name="translatedText">翻訳文</param>
        /// <returns>検索対象の可否</returns>
        private bool IsValidate(string originalText, string translatedText)
        {
            if (originalText.Equals(translatedText, StringComparison.OrdinalIgnoreCase))
            {
                //// 無効：原文と翻訳文が同一
                return false;
            }
            else if (string.IsNullOrWhiteSpace(originalText))
            {
                //// 無効：原文が空行
                return false;
            }
            else if (string.IsNullOrWhiteSpace(translatedText))
            {
                //// 無効：翻訳文が空行
                return false;
            }

            //// 有効：検索対象テキストである
            return true;
        }
    }
}

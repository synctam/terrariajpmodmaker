namespace LibTrSearcher.Models.LanguageData
{
    using System.Text;

    /// <summary>
    /// 言語エントリー
    /// </summary>
    public class TrsLangDataEntry
    {
        private string originalText = string.Empty;
        private string translatedText = string.Empty;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="key">キー</param>
        /// <param name="originalText">原文</param>
        /// <param name="translatedText">翻訳文</param>
        public TrsLangDataEntry(string key, string originalText, string translatedText)
        {
            this.Key = key;
            this.originalText = originalText;
            this.translatedText = translatedText;
        }

        /// <summary>
        /// キー
        /// </summary>
        public string Key { get; } = string.Empty;

        /// <summary>
        /// 原文
        /// </summary>
        public string OriginalText
        {
            get
            {
                return this.GetControlChar(this.originalText);
            }

            set
            {
                this.originalText = value;
            }
        }

        /// <summary>
        /// 翻訳文
        /// </summary>
        public string TranslatedText
        {
            get
            {
                return this.GetControlChar(this.translatedText);
            }

            set
            {
                this.translatedText = value;
            }
        }

        /// <summary>
        /// 制御文字化したテキストを返す。
        /// </summary>
        /// <param name="text">テキスト</param>
        /// <returns>制御文字化したテキスト</returns>
        private string GetControlChar(string text)
        {
            var buff = new StringBuilder(text);

            buff.Replace(@"\n", "\r\n");

            return buff.ToString();
        }
    }
}

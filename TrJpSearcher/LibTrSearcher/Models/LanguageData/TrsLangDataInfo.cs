namespace LibTrSearcher.Models.LanguageData
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// 言語情報
    /// </summary>
    public class TrsLangDataInfo
    {
        /// <summary>
        /// 言語ファイルの辞書
        /// キーは FileID
        /// </summary>
        public Dictionary<string, TrsLangDataFile> Items { get; } =
            new Dictionary<string, TrsLangDataFile>(StringComparer.CurrentCultureIgnoreCase);

        /// <summary>
        /// 言語エントリーの追加
        /// </summary>
        /// <param name="fileID">FileID</param>
        /// <param name="group">グループ</param>
        /// <param name="key">キー</param>
        /// <param name="originalText">原文</param>
        /// <param name="translatedText">翻訳文</param>
        public void AddEntry(
            string fileID,
            string group,
            string key,
            string originalText,
            string translatedText)
        {
            if (this.Items.ContainsKey(fileID))
            {
                var currentFile = this.Items[fileID];
                currentFile.AddEntry(group, key, originalText, translatedText);
            }
            else
            {
                var newFile = new TrsLangDataFile(fileID);
                this.Items.Add(fileID, newFile);

                newFile.AddEntry(group, key, originalText, translatedText);
            }
        }

        /// <summary>
        /// 検索テキストで検索し、結果を検索結果のリストに追加する。
        /// </summary>
        /// <param name="results">検索結果のリスト</param>
        /// <param name="text">検索テキスト</param>
        public void Search(TrsLangDataInfo results, string text)
        {
            foreach (var langDataFile in this.Items.Values)
            {
                langDataFile.Search(results, text);
            }
        }
    }
}

namespace LibTrSearcher.Models.LanguageData
{
    using System;
    using System.ComponentModel;
    using System.Reactive.Disposables;
    using Reactive.Bindings;

    /// <summary>
    /// 翻訳シート
    /// </summary>
    public class TrsLangDataFlatEntry : INotifyPropertyChanged, IDisposable
    {
        //// Dispose が必要な ReactiveProperty や ReactiveCommand を記録する
        private readonly CompositeDisposable disposables = new CompositeDisposable();

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public TrsLangDataFlatEntry()
        {
            this.IsSelected = new ReactiveProperty<bool>();
        }

        /// <summary>
        /// プロパティ変更通知
        /// 明示的には使用しない
        /// </summary>
#pragma warning disable CS0067
        public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore CS0067

        /// <summary>
        /// FileID
        /// </summary>
        public string FileID { get; set; } = string.Empty;

        /// <summary>
        /// グループ
        /// </summary>
        public string Group { get; set; } = string.Empty;

        /// <summary>
        /// キー
        /// </summary>
        public string Key { get; set; } = string.Empty;

        /// <summary>
        /// 原文
        /// </summary>
        public string OriginalText { get; set; } = string.Empty;

        /// <summary>
        /// 翻訳文
        /// </summary>
        public string TranslatedText { get; set; } = string.Empty;

        /// <summary>
        /// 選択された項目
        /// </summary>
        public ReactiveProperty<bool> IsSelected { get; } = null;

        /// <summary>
        /// 後処理
        /// </summary>
        public void Dispose()
        {
            this.disposables.Dispose();
        }
    }
}

namespace LibTrSearcher.Models.TransSheet
{
    using System.Globalization;
    using System.IO;
    using System.Text;
    using CsvHelper;
    using CsvHelper.Configuration;
    using LibTrSearcher.Models.LanguageData;

    /// <summary>
    /// 翻訳シート入出力
    /// </summary>
    public class TrsTransSheetDao
    {
        /// <summary>
        /// 翻訳シートを読み込み言語情報を返す。
        /// </summary>
        /// <param name="path">翻訳シートのパス</param>
        /// <returns>言語情報</returns>
        public static TrsLangDataInfo LoadFromCsv(string path)
        {
            var fullPath = Path.GetFullPath(path);
            var result = new TrsLangDataInfo();
            var enc = Encoding.UTF8;

            using (var reader = new StreamReader(fullPath, enc))
            using (var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csvReader.Configuration.Delimiter = ",";
                csvReader.Configuration.HasHeaderRecord = true;
                csvReader.Configuration.RegisterClassMap<CsvMapper>();
                var records = csvReader.GetRecords<TrsLangDataFlatEntry>();

                foreach (var entry in records)
                {
                    result.AddEntry(
                        entry.FileID,
                        entry.Group,
                        entry.Key,
                        entry.OriginalText,
                        entry.TranslatedText);
                }

                return result;
            }
        }

        /// <summary>
        /// CSVファイルのマッピング
        /// </summary>
        public class CsvMapper : ClassMap<TrsLangDataFlatEntry>
        {
            /// <summary>
            /// コンストラクタ
            /// </summary>
            public CsvMapper()
            {
                this.Map((x) => x.FileID).Name("[[FileID]]");
                this.Map((x) => x.Group).Name("[[Group]]");
                this.Map((x) => x.Key).Name("[[Key]]");
                this.Map((x) => x.OriginalText).Name("[[English]]");
                this.Map((x) => x.TranslatedText).Name("[[Japanese]]");
            }
        }
    }
}

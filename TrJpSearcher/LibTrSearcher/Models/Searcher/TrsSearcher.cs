namespace LibTrSearcher.Models.Searcher
{
    using LibTrSearcher.Models.LanguageData;
    using LibTrSearcher.Models.TransSheet;

    /// <summary>
    /// 検索
    /// </summary>
    public class TrsSearcher
    {
        /// <summary>
        /// 言語情報
        /// </summary>
        public TrsLangDataInfo LangDataInfo { get; private set; } = null;

        /// <summary>
        /// 翻訳シートの読み込み
        /// </summary>
        /// <param name="path">翻訳シートのパス</param>
        public void LoadFromCsv(string path)
        {
            this.LangDataInfo = TrsTransSheetDao.LoadFromCsv(path);
        }
    }
}

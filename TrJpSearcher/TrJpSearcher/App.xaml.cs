namespace TrJpSearcher
{
    using System;
    using System.IO;
    using System.Windows;
    using TrJpSearcher.MainForm;
    using TrJpSearcher.Nucleus;
    using TrJpSearcher.Properties;

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private TrsNucleus nucleus = null;
        private MainWindow mainWindow = null;
        private int exitCode = 0;

        /// <summary>
        /// [イベント] アプリケーション起動時の処理
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            var transSheetPath = Settings.Default.TerrariaTranslationSheetPath;
#if DEBUG
            transSheetPath = @"..\..\..\..\data\csv\TerrariaTranslationSheet.csv";
#endif
            transSheetPath = Path.GetFullPath(transSheetPath);
            if (File.Exists(transSheetPath))
            {
                //// ルートモデルの生成し、翻訳シートを読み込む
                this.nucleus = new TrsNucleus();

                this.nucleus.LoadFromCsv(transSheetPath);

                //// ViewModelの生成とルートモデルの割り当て
                var mainWindowViewModel = new MainWindowViewModel(this.nucleus);

                //// Viewの生成とViewModelの割り当て
                this.mainWindow = new MainWindow();
                this.mainWindow.DataContext = mainWindowViewModel;
                this.mainWindow.Init();

                //// メインフォーム表示
                this.mainWindow.Show();
            }
            else
            {
                //// 翻訳シートが見つからない時は、エラーを表示しアプリケーションの終了を宣言する。
                var msg =
                    $"Terraria text viewer の起動に失敗しました。{Environment.NewLine}" +
                    $"翻訳シートが見つかりません。{Environment.NewLine}" +
                    $"ファイル名：{transSheetPath}{Environment.NewLine}{Environment.NewLine}" +
                    $"このツールは Terraria JpMod Maker(TMM) と同じフォルダーにインストールする必要があります。";
                MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);

                this.exitCode = 1;
                this.Shutdown(this.exitCode);
            }
        }

        /// <summary>
        /// [イベント] アプリケーション終了時の処理
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void Application_Exit(object sender, ExitEventArgs e)
        {
            //// Application_Startup のエラーの有無に関わらずこのメソッドはコールされる。
            if (this.exitCode == 0)
            {
                this.mainWindow.Close();
            }

            e.ApplicationExitCode = this.exitCode;
        }
    }
}

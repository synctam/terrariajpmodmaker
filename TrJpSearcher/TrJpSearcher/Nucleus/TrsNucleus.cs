namespace TrJpSearcher.Nucleus
{
    using LibTrSearcher.Models.LanguageData;
    using LibTrSearcher.Models.Searcher;

    /// <summary>
    /// ルートモデル
    /// </summary>
    public class TrsNucleus
    {
        private TrsSearcher searcher = new TrsSearcher();

        /// <summary>
        /// 翻訳シートの読み込み
        /// </summary>
        /// <param name="path">翻訳シートのパス</param>
        public void LoadFromCsv(string path)
        {
            this.searcher.LoadFromCsv(path);
        }

        /// <summary>
        /// 検索テキストを検索結果リストを返す
        /// </summary>
        /// <param name="searchresultsInfo">検索結果情報</param>
        /// <param name="text">検索テキスト</param>
        public void Search(TrsLangDataInfo searchresultsInfo, string text)
        {
            this.searcher.LangDataInfo.Search(searchresultsInfo, text);
        }
    }
}

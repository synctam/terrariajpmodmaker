namespace TrJpSearcher.MainForm
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Windows;
    using System.Windows.Input;

    /// <summary>
    /// フォーカス管理
    /// </summary>
    public static class MainFormFocusExtension
    {
        /// <summary>
        /// IsFocused の定義
        /// </summary>
        public static readonly DependencyProperty IsFocusedProperty =
            DependencyProperty.RegisterAttached(
                "IsFocused",
                typeof(bool),
                typeof(MainFormFocusExtension),
                new UIPropertyMetadata(false, OnIsFocusedPropertyChanged));

        /// <summary>
        /// objのフォーカスの有無を返す
        /// </summary>
        /// <param name="obj">obj</param>
        /// <returns>フォーカスの有無</returns>
        public static bool GetIsFocused(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsFocusedProperty);
        }

        /// <summary>
        /// objにフォーカスの有無を設定する
        /// </summary>
        /// <param name="obj">obj</param>
        /// <param name="value">フォーカスの有無</param>
        public static void SetIsFocused(DependencyObject obj, bool value)
        {
            obj.SetValue(IsFocusedProperty, value);
            //////obj.SetValue(InputMethod.IsInputMethodEnabledProperty, false);
            //////obj.SetValue(InputMethod.PreferredImeStateProperty, InputMethodState.Off);
            //////obj.SetValue(InputMethod.PreferredImeConversionModeProperty, ImeConversionModeValues.NoConversion);
        }

        /// <summary>
        /// objにフォーカスを設定する
        /// </summary>
        /// <param name="d">対象のオブジェクト</param>
        /// <param name="e">e</param>
        private static void OnIsFocusedPropertyChanged(
            DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            var uie = (UIElement)d;
            if ((bool)e.NewValue)
            {
                uie.Focus();
            }
        }
    }
}

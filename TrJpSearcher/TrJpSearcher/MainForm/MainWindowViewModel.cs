namespace TrJpSearcher.MainForm
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Reactive.Disposables;
    using System.Reactive.Linq;
    using System.Runtime.InteropServices;
    using System.Web;
    using System.Windows;
    using LibTrSearcher.Models.LanguageData;
    using Reactive.Bindings;
    using Reactive.Bindings.Extensions;
    using TrJpSearcher.Nucleus;

    /// <summary>
    /// [ViewModel] メインウィンドウ
    /// </summary>
    public class MainWindowViewModel : INotifyPropertyChanged, IDisposable
    {
        //// Dispose が必要な ReactiveProperty や ReactiveCommand を記録する
        private readonly CompositeDisposable disposables = new CompositeDisposable();

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="nucleus">ルートモデル</param>
        public MainWindowViewModel(TrsNucleus nucleus)
        {
            //// 通知クラスの生成と破棄リストへの登録
            this.Title = new ReactiveProperty<string>().AddTo(this.disposables);
            this.Title.Value = "Terraria text view";
            this.SearchText = new ReactiveProperty<string>().AddTo(this.disposables);
            this.SelectedItem = new ReactiveProperty<TrsLangDataFlatEntry>().AddTo(this.disposables);
            this.IsSelected = new ReactiveProperty<bool>(false).AddTo(this.disposables);

            //// 検索結果のコレクション
            this.SearchedLangCollections = new ReactiveCollection<TrsLangDataFlatEntry>().AddTo(this.disposables);

            //// コピーコマンド
            this.CommandCopy = new ReactiveCommand().AddTo(this.disposables);
            //// ネットサーチコマンド（日本語wiki検索）
            this.CommandNetSearchJapanese = new ReactiveCommand().AddTo(this.disposables);
            //// ネットサーチコマンド（英語wiki検索）
            this.CommandNetSearchEnglish = new ReactiveCommand().AddTo(this.disposables);

            //// [通知登録] 検索ボックスが変更された時に実行する処理を登録
            this.SearchText.Subscribe(x =>
            {
                if (string.IsNullOrWhiteSpace(x))
                {
                    //// 検索文字列が空の時は処理不要
                    this.SearchedLangCollections.Clear();
                    return;
                }

                var searchresultsInfo = new TrsLangDataInfo();
                this.SearchedLangCollections.Clear();

                nucleus.Search(searchresultsInfo, x);
                foreach (var langDataFile in searchresultsInfo.Items.Values)
                {
                    foreach (var langDataGroup in langDataFile.Items.Values)
                    {
                        foreach (var langDataEntry in langDataGroup.Items.Values)
                        {
                            var flatEntry = new TrsLangDataFlatEntry();
                            flatEntry.FileID = langDataFile.FileID;
                            flatEntry.Group = langDataGroup.Group;
                            flatEntry.Key = langDataEntry.Key;
                            flatEntry.OriginalText = langDataEntry.OriginalText;
                            flatEntry.TranslatedText = langDataEntry.TranslatedText;
                            //// [通知登録] この項目が選択された時の処理を登録する
                            flatEntry.IsSelected.Subscribe(x =>
                            {
                                if (x)
                                {
                                    //// 選択された項目を保存する
                                    this.SelectedItem.Value = flatEntry;
                                }
                            });

                            this.SearchedLangCollections.Add(flatEntry);
                        }
                    }
                }
            });

            //// [通知登録] 検索結果の選択項目が変更された時に実行する処理を登録する。
            this.CommandCopy.Subscribe(() =>
            {
                if (this.SelectedItem.Value != null)
                {
                    var originalText = this.SelectedItem.Value.OriginalText;
                    Clipboard.SetData(DataFormats.Text, originalText);
#if DEBUG
                    Debug.Print($"Execute command: copy. Key({this.SelectedItem.Value.Key}) Text({this.SelectedItem.Value.OriginalText})");
#endif
                }
            });

            //// [通知登録] 日本語wiki検索を指示されたときに実行する処理を登録する。
            this.CommandNetSearchJapanese.Subscribe(() =>
            {
                if (this.SelectedItem.Value != null)
                {
                    var url = "http://terraria.arcenserv.info/w/index.php";
                    this.ExecuteWikiSearch(url, this.SelectedItem.Value.OriginalText);
                }
            });

            //// [通知登録] 英語wiki検索を指示されたときに実行する処理を登録する。
            this.CommandNetSearchEnglish.Subscribe(() =>
            {
                if (this.SelectedItem.Value != null)
                {
                    var url = "https://terraria.gamepedia.com/index.php";
                    this.ExecuteWikiSearch(url, this.SelectedItem.Value.OriginalText);
                }
            });

            //// [通知登録] 検索結果の選択項目が変更された時に実行する処理を登録する。
            this.SelectedItem.Subscribe(x =>
            {
                if (x == null)
                {
                    return;
                }

#if DEBUG
                Debug.Print($"Group({x.Group}) Key({x.Key}) Text({x.OriginalText})");
#endif
            });
        }

        /// <summary>
        /// プロパティ変更通知
        /// 明示的には使用しない
        /// </summary>
#pragma warning disable CS0067
        public event PropertyChangedEventHandler PropertyChanged;
        //// [StyleCop] 変数未使用のワーニングを抑止する
#pragma warning restore CS0067

        /// <summary>
        /// タイトル
        /// </summary>
        public ReactiveProperty<string> Title { get; } = null;

        /// <summary>
        /// 検索キーワード
        /// </summary>
        public ReactiveProperty<string> SearchText { get; } = null;

        /// <summary>
        /// ListViteのアイテム選択の有無
        /// </summary>
        public ReactiveProperty<bool> IsSelected { get; } = null;

        /// <summary>
        /// 検索結果のコレクション
        /// </summary>
        public ReactiveCollection<TrsLangDataFlatEntry> SearchedLangCollections { get; } = null;

        /// <summary>
        /// [コマンド] 選択した項目の原文をクリップボードにコピーする
        /// </summary>
        public ReactiveCommand CommandCopy { get; } = null;

        /// <summary>
        /// [コマンド] wikiを起動し選択された項目を検索する
        /// </summary>
        public ReactiveCommand CommandNetSearchJapanese { get; } = null;

        /// <summary>
        /// [コマンド] wikiを起動し選択された項目を検索する
        /// </summary>
        public ReactiveCommand CommandNetSearchEnglish { get; } = null;

        /// <summary>
        /// 選択された項目
        /// </summary>
        public ReactiveProperty<TrsLangDataFlatEntry> SelectedItem { get; } = null;

        /// <summary>
        /// 後処理
        /// </summary>
        public void Dispose()
        {
            //// ReactiveProperty で通知登録した処理を破棄する。
            this.disposables.Dispose();
        }

        /// <summary>
        /// ブラウザーを起動し、Tarraria wiki で検索文字列を検索する。
        /// </summary>
        /// <param name="url">wikiのURL</param>
        /// <param name="searchText">検索文字列</param>
        private void ExecuteWikiSearch(string url, string searchText)
        {
            var originalText = this.SelectedItem.Value.OriginalText;

            var urlEncordedSearchText = HttpUtility.UrlEncode(originalText);
            var uri = $"{url}?search={urlEncordedSearchText}";

            if (RuntimeInformation.FrameworkDescription.Contains(
                ".NET Core", StringComparison.OrdinalIgnoreCase))
            {
                //// .Net core で実行した場合
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                {
                    // Windows
                    uri = uri.Replace("&", "^&");
                    Process.Start(new ProcessStartInfo(
                         "cmd",
                         $"/c start {uri}")
                    { CreateNoWindow = true });
                }
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                {
                    // [未検証] Linux
                    Process.Start("xdg-open", uri);
                }
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                {
                    // [未検証] Mac
                    Process.Start("open", uri);
                }
                else
                {
                    throw new Exception($"Unknowwn Framework({RuntimeInformation.FrameworkDescription})");
                }
            }
            else
            {
                //// .Net Framework で実行した場合
                Process.Start(uri);
            }
        }
    }
}

namespace TrJpSearcher.MainForm
{
    using System;
    using System.Diagnostics;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using TrJpSearcher.Properties;

    /// <summary>
    /// メインウィンドウ
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// メインウィンドウ
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
        }

        private enum NListVirwUpDownDirection
        {
            Idle,
            Up,
            Down,
        }

        /// <summary>
        /// 画面の初期化
        /// </summary>
        public void Init()
        {
            //// ウィンドウの位置とサイズを復元する
            this.SetWindowPos();
            //// 検索ボックスにフォーカスが移動した時にテキストを選択状態にする。
            this.SearchTextBox.GotFocus += (sender, e) =>
                this.Dispatcher.InvokeAsync(() =>
                {
                    Task.Delay(0);
                    ((TextBox)sender).SelectAll();
                });

            this.SearchTextBox.Focus();
        }

        /// <summary>
        /// [イベント] ウィンドウの位置とサイズを保存する
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.SaveWindowPos();
            e.Cancel = false;
        }

        /// <summary>
        /// ListView上で　Ctrl+F 押下時、検索テキストボックスにフォーカスを移動する。
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void ListView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
            {
                this.SearchTextBox.Focus();
            }
        }

        /// <summary>
        /// ウインドウの位置とサイズを復元する。
        /// </summary>
        private void SetWindowPos()
        {
            //// 設定情報を読み込む
            var settings = Settings.Default;
            if ((settings.MainWindowPosition.Width <= 0.0) ||
                (settings.MainWindowPosition.Height <= 0.0))
            {
                //// 初期値を設定する
                this.Width = 800;
                this.Height = 450;
            }
            else
            {
                //// 設定値を元に復元する
                this.Width = settings.MainWindowPosition.Width;
                this.Height = settings.MainWindowPosition.Height;
            }

            this.Left = settings.MainWindowPosition.Left;
            this.Top = settings.MainWindowPosition.Top;
            this.WindowState = settings.MainWindowState;
        }

        /// <summary>
        /// ウインドウの位置とサイズを保存する。
        /// </summary>
        private void SaveWindowPos()
        {
            var mainWindowRect = new Rect(
                this.Left,
                this.Top,
                this.Width,
                this.Height);

            var settings = Settings.Default;

            settings.MainWindowPosition = mainWindowRect;
            settings.MainWindowState = this.WindowState;

            settings.Save();
        }

        private void UpDownListViewItem(NListVirwUpDownDirection dir)
        {
            switch (dir)
            {
                case NListVirwUpDownDirection.Idle:
                    break;
                case NListVirwUpDownDirection.Down:
                    KeyEventArgs keyEventDown = new KeyEventArgs(
                        Keyboard.PrimaryDevice,
                        PresentationSource.FromVisual(this.ListViewLangage),
                        0,
                        Key.Down);
                    this.DoUpDownListViewItem(keyEventDown);
                    break;
                case NListVirwUpDownDirection.Up:
                    KeyEventArgs keyEventUp = new KeyEventArgs(
                        Keyboard.PrimaryDevice,
                        PresentationSource.FromVisual(this.ListViewLangage),
                        0,
                        Key.Up);
                    this.DoUpDownListViewItem(keyEventUp);
                    break;
                default:
                    throw new Exception($"Unknown error");
            }
        }

        private void DoUpDownListViewItem(KeyEventArgs keyEvent)
        {
            keyEvent.RoutedEvent = Keyboard.KeyDownEvent;
            this.ListViewLangage.RaiseEvent(keyEvent);
        }
    }
}

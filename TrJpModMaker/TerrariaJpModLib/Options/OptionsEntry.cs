namespace TrJpModMaker.Models.Options
{
    using System;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Text;
    using System.Xml.Serialization;
    using TrJpModMaker.Models.HashUtils;

    public class OptionsEntry
    {
        private const int DefaultSerialNo = 0;
        private const string DefaultOptionFileName = "TrJpModMaker.xml";
        private const string DefaultTrrrariaJpExeName = "Terraria_jp.exe";
        private const string DefaultTrrrariaEnExeName = "Terraria.exe";
        private const string DefaultTransSheetPath = "data\\csv\\TerrariaTranslationSheet.csv";
        private const string DefaultGameFontsFolderName = "Content\\Fonts";
        private const string DefaultBkupFontsFolderName = "FontsBkup";
        private const string DefaultJapaneseFontsFolderName = "Fonts";
        private static OptionsEntry instance = new OptionsEntry();

        public enum NOptionsStatus
        {
            Idole,
            Loaded,
            NotFound
        }

        public int SerialNo { get; set; }

        public string TerrariaExePath { get; set; } = string.Empty;

        public string TerrariaExeHash { get; set; } = string.Empty;

        public string FileVersion { get; set; } = string.Empty;

        public bool NeedUpdate { get; set; }

        public bool UseUntranslationTable { get; set; } = false;

        /// <summary>
        /// 非翻訳ファイル（チャットコマンド）の使用有無。
        /// true: 使用する（翻訳しない）。
        /// デフォルトはチャットコマンドは翻訳しない。
        /// </summary>
        public bool UseUntranslationCommand { get; set; } = true;

        [XmlIgnore]
        public Bitmap IconImage { get; set; }

        [XmlIgnore]
        public NOptionsStatus State { get; set; }

        [XmlIgnore]
        public string TerrariaJpExeName { get; } = DefaultTrrrariaJpExeName;

        [XmlIgnore]
        public string TerrariaEnExeName { get; } = DefaultTrrrariaEnExeName;

        [XmlIgnore]
        public string TransSheetPath { get; } = DefaultTransSheetPath;

        [XmlIgnore]
        public string TerrariaJpExePath
        {
            get
            {
                return Path.Combine(Path.GetDirectoryName(this.TerrariaExePath), this.TerrariaJpExeName);
            }
        }

        [XmlIgnore]
        public string TerrariaGameFolderPath
        {
            get
            {
                string directoryName = Path.GetDirectoryName(this.TerrariaExePath);
                if (string.IsNullOrEmpty(directoryName))
                {
                    return string.Empty;
                }

                if (Directory.Exists(directoryName))
                {
                    return directoryName;
                }

                return string.Empty;
            }
        }

        [XmlIgnore]
        public string TerrariaGameFontsFolderPath
        {
            get
            {
                string text = Path.Combine(this.TerrariaGameFolderPath, "Content\\Fonts");
                if (string.IsNullOrEmpty(text))
                {
                    return string.Empty;
                }

                if (Directory.Exists(text))
                {
                    return text;
                }

                return string.Empty;
            }
        }

        [XmlIgnore]
        public string TerrariaJapaneseFontsFolderPath
        {
            get
            {
                string text = "Fonts";
                if (string.IsNullOrEmpty(text))
                {
                    return string.Empty;
                }

                if (Directory.Exists(text))
                {
                    return text;
                }

                return string.Empty;
            }
        }

        [XmlIgnore]
        public string TerrariaBkupFontsFolderPath
        {
            get
            {
                string text = "FontsBkup";
                if (string.IsNullOrEmpty(text))
                {
                    return string.Empty;
                }

                if (Directory.Exists(text))
                {
                    return text;
                }

                Directory.CreateDirectory(text);
                return text;
            }
        }

        public static OptionsEntry GetInstance()
        {
            return instance;
        }

        public bool ExistsTerrariaJpExe()
        {
            if (File.Exists(this.TerrariaJpExePath))
            {
                return true;
            }

            return false;
        }

        public void LoadFromXml()
        {
            if (File.Exists("TrJpModMaker.xml"))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(OptionsEntry));
                using (StreamReader textReader = new StreamReader("TrJpModMaker.xml", new UTF8Encoding(false)))
                {
                    OptionsEntry optionsEntry = (OptionsEntry)xmlSerializer.Deserialize(textReader);
                    this.FileVersion = optionsEntry.FileVersion;
                    this.NeedUpdate = optionsEntry.NeedUpdate;
                    this.SerialNo = optionsEntry.SerialNo;
                    this.TerrariaExeHash = optionsEntry.TerrariaExeHash;
                    this.TerrariaExePath = optionsEntry.TerrariaExePath;
                    this.State = NOptionsStatus.Loaded;
                    this.UseUntranslationTable = optionsEntry.UseUntranslationTable;
                    this.UseUntranslationCommand = optionsEntry.UseUntranslationCommand;
                }

                return;
            }

            this.State = NOptionsStatus.NotFound;
            throw new FileNotFoundException(string.Format("オプションファイルが存在しません。File({0})", "TrJpModMaker.xml"));
        }

        public void LoadFromTerrariaExePath(string terrariaExePath)
        {
            this.CheckTerrariaExe(terrariaExePath);
            string hash = GetHash(terrariaExePath);
            switch (this.State)
            {
                case NOptionsStatus.Idole:
                    this.NeedUpdate = true;
                    this.State = NOptionsStatus.Loaded;
                    break;
                case NOptionsStatus.Loaded:
                    if (!(this.TerrariaExeHash == hash))
                    {
                        this.NeedUpdate = true;
                    }

                    break;
                case NOptionsStatus.NotFound:
                    this.NeedUpdate = false;
                    this.State = NOptionsStatus.Loaded;
                    break;
                default:
                    throw new Exception("想定外のエラー");
            }

            this.FileVersion = GetVersion(terrariaExePath);
            this.IconImage = GetIcon(terrariaExePath);
            this.SerialNo = 0;
            this.TerrariaExeHash = hash;
            this.TerrariaExePath = terrariaExePath;
        }

        public void SaveToXml()
        {
            if (this.State == NOptionsStatus.Loaded)
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(OptionsEntry));
                using (StreamWriter textWriter = new StreamWriter("TrJpModMaker.xml", false, new UTF8Encoding(false)))
                {
                    xmlSerializer.Serialize(textWriter, instance);
                }
            }
        }

        public void CheckTerrariaExe(string terrariaExePath)
        {
            if (string.IsNullOrEmpty(Path.GetFileName(terrariaExePath)))
            {
                StringBuilder buff1 = new StringBuilder();
                buff1.AppendLine("TerrariaのEXEが指定されていません。");
                buff1.AppendLine();
                buff1.AppendLine(string.Format("{0}を指定してください。", this.TerrariaEnExeName));
                throw new InvalidOptionsPathException(buff1.ToString());
            }

            if (Path.GetFileName(terrariaExePath).ToLower() == this.TerrariaEnExeName.ToLower())
            {
                if (File.Exists(terrariaExePath))
                {
                    return;
                }

                StringBuilder buff2 = new StringBuilder();
                buff2.AppendLine(string.Format("{0}が存在しません。", this.TerrariaEnExeName));
                buff2.AppendLine(string.Format("正しい{0}を指定してください。", this.TerrariaEnExeName));
                throw new FileNotFoundException(buff2.ToString());
            }

            StringBuilder buff3 = new StringBuilder();
            buff3.AppendLine("TerrariaのEXEではありません。");
            buff3.AppendLine(terrariaExePath);
            buff3.AppendLine();
            buff3.AppendLine(string.Format("正しい{0}を指定してください。", this.TerrariaEnExeName));
            throw new InvalidOptionsPathException(buff3.ToString());
        }

        private static string GetHash(string path)
        {
            FileVersionInfo.GetVersionInfo(path);
            return FileHash.CalcHash(path);
        }

        private static string GetVersion(string path)
        {
            FileVersionInfo versionInfo = FileVersionInfo.GetVersionInfo(path);
            return string.Format("{0}", versionInfo.FileVersion);
        }

        private static Bitmap GetIcon(string path)
        {
            if (File.Exists(path))
            {
                Icon icon = Icon.ExtractAssociatedIcon(path);
                if (icon != null)
                {
                    return icon.ToBitmap();
                }

                return null;
            }

            return null;
        }

        public class InvalidOptionsPathException : Exception
        {
            public InvalidOptionsPathException(string msg)
                : base(msg)
            {
            }
        }
    }
}

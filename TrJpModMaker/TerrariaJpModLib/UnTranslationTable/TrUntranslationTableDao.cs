namespace TerrariaJpModLib.UnTranslationTable
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using CsvHelper;
    using CsvHelper.Configuration;

    public class TrUntranslationTableDao
    {
        public static void LoadFromCsv(TrUntranslationTableInfo transInfo, string path, Encoding enc = null)
        {
            var encoding = new UTF8Encoding(false);
            enc = enc ?? encoding;
            using (var reader = new StreamReader(path, enc))
            {
                using (var csvReader = new CsvReader(reader))
                {
                    csvReader.Configuration.Delimiter = ",";
                    csvReader.Configuration.HasHeaderRecord = true;
                    csvReader.Configuration.RegisterClassMap<CsvMapper>();
                    csvReader.Configuration.RegisterClassMap<CsvMapper>();

                    var records = csvReader.GetRecords<TrUntranslationTableEntry>();
                    foreach (var entry in records)
                    {
                        transInfo.AddEntry(entry);
                    }
                }
            }
        }

        public static void SaveToCsv(TrUntranslationTableInfo transInfo, string path)
        {
            using (var writer = new CsvWriter(new StreamWriter(path, false, Encoding.UTF8)))
            {
                writer.Configuration.RegisterClassMap<CsvMapper>();
                writer.Configuration.HasHeaderRecord = true;
                writer.WriteHeader<TrUntranslationTableEntry>();
                writer.NextRecord();

                foreach (var entry in transInfo.Items.Values)
                {
                    writer.WriteRecord(entry);
                    writer.NextRecord();
                }
            }
        }

#if WINDOWS_7
        public class CsvMapper : CsvClassMap<TrUntranslationTableEntry>
#else
        public class CsvMapper : ClassMap<TrUntranslationTableEntry>
#endif
        {
            public CsvMapper()
            {
                this.Map((x) => x.FileID).Name("[[FileID]]");
                this.Map((x) => x.Group).Name("[[Group]]");
                this.Map((x) => x.IsUntranslation).Name("[[IsUntranslation]]");
            }
        }
    }
}

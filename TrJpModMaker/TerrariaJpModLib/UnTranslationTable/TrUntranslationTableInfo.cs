namespace TerrariaJpModLib.UnTranslationTable
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class TrUntranslationTableInfo
    {
        private const char Delimiter = ':';

        /// <summary>
        /// 非翻訳グループの辞書
        /// キーは FileID:Group
        /// </summary>
        public SortedDictionary<string, TrUntranslationTableEntry> Items { get; } =
            new SortedDictionary<string, TrUntranslationTableEntry>();

        public void AddEntry(TrUntranslationTableEntry entry)
        {
            var key = this.MakeKey(entry.FileID, entry.Group);
            if (this.Items.ContainsKey(key))
            {
                //// 既に登録済みの場合は更新する。
                this.Items[key].IsUntranslation = entry.IsUntranslation;
                Console.WriteLine($"Duplicate key({key})");
            }
            else
            {
                this.Items.Add(key, entry);
            }
        }

        /// <summary>
        /// 翻訳グループの翻訳可否を返す。
        /// </summary>
        /// <param name="fileID">FileID</param>
        /// <param name="group">group</param>
        /// <returns>翻訳可否。true: 翻訳する</returns>
        public bool IsTranslation(string fileID, string group)
        {
            fileID = fileID + ".json";
            var key = this.MakeKey(fileID, group);
            if (this.Items.ContainsKey(key))
            {
                return !this.Items[key].IsUntranslation;
            }
            else
            {
                return true;
            }
        }

        private string MakeKey(string fileID, string group)
        {
            return $"{fileID}{Delimiter}{group}";
        }

        private string GetFileIDFromKey(string key)
        {
            var keys = key.Split(Delimiter);
            if (keys.Length == 2)
            {
                return keys[0];
            }
            else
            {
                return string.Empty;
            }
        }

        private string GetGroupFromKey(string key)
        {
            var keys = key.Split(Delimiter);
            if (keys.Length == 2)
            {
                return keys[1];
            }
            else
            {
                return string.Empty;
            }
        }
    }
}

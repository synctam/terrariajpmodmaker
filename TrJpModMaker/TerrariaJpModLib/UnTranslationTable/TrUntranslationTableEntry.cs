namespace TerrariaJpModLib.UnTranslationTable
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class TrUntranslationTableEntry
    {
        public string FileID { get; set; } = string.Empty;

        public string Group { get; set; } = string.Empty;

        public bool IsUntranslation { get; set; } = false;
    }
}

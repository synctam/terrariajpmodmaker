namespace TerrariaJpModLib.UnRegistrationTable
{
    using System.IO;
    using System.Text;
    using CsvHelper;
    using CsvHelper.Configuration;

    public class TrUnRegistrationTableDao
    {
        public static void LoadFromCsv(TrUnRegistrationTableInfo transInfo, string path, Encoding enc = null)
        {
            var encoding = new UTF8Encoding(false);
            enc = enc ?? encoding;
            using (var reader = new StreamReader(path, enc))
            {
                using (var csvReader = new CsvReader(reader))
                {
                    csvReader.Configuration.Delimiter = ",";
                    csvReader.Configuration.HasHeaderRecord = true;
                    csvReader.Configuration.RegisterClassMap<CsvMapper>();
                    csvReader.Configuration.RegisterClassMap<CsvMapper>();

                    var records = csvReader.GetRecords<TrUnRegistrationTableEntry>();
                    foreach (var entry in records)
                    {
                        transInfo.AddEntry(entry);
                    }
                }
            }
        }

        public class CsvMapper : ClassMap<TrUnRegistrationTableEntry>
        {
            public CsvMapper()
            {
                this.Map((x) => x.FileID).Name("[[FileID]]");
                this.Map((x) => x.Group).Name("[[Group]]");
            }
        }
    }
}

namespace TerrariaJpModLib.UnRegistrationTable
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// ParaTransに登録しない FildID と Group を管理する。
    /// </summary>
    public class TrUnRegistrationTableInfo
    {
        private const char Delimiter = ':';

        /// <summary>
        /// 非登録グループの辞書
        /// キーは FileID:Group
        /// </summary>
        public SortedDictionary<string, TrUnRegistrationTableEntry> Items { get; } =
            new SortedDictionary<string, TrUnRegistrationTableEntry>();

        public void AddEntry(TrUnRegistrationTableEntry entry)
        {
            var key = this.MakeKey(entry.FileID, entry.Group);
            if (this.Items.ContainsKey(key))
            {
                //// 既に登録済みの場合はそのまま。
                Console.WriteLine($"Duplicate key({key})");
            }
            else
            {
                this.Items.Add(key, entry);
            }
        }

        /// <summary>
        /// 非翻訳グループかどうかを返す。
        /// </summary>
        /// <param name="fileID">FileID</param>
        /// <param name="group">group</param>
        /// <returns>非翻訳グループの場合は true</returns>
        public bool IsUnRegistration(string fileID, string group)
        {
            var key = this.MakeKey(fileID, group);
            return this.Items.ContainsKey(key);
        }

        private string MakeKey(string fileID, string group)
        {
            return $"{fileID}{Delimiter}{group}";
        }
    }
}

namespace TerrariaJpModLib.UnRegistrationTable
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class TrUnRegistrationTableEntry
    {
        public string FileID { get; set; } = string.Empty;

        public string Group { get; set; } = string.Empty;
    }
}

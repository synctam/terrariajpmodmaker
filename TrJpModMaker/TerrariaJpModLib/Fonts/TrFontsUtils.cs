namespace TrJpModMaker.Models.Fonts
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using TerrariaJpModLib.Utils;
    using TrJpModMaker.Models.HashUtils;
    using TrJpModMaker.Models.Options;

    public class TrFontsUtils
    {
        private static string[] fontFiles = new string[5]
        {
            "Combat_Crit.xnb",
            "Combat_Text.xnb",
            "Death_Text.xnb",
            "Item_Stack.xnb",
            "Mouse_Text.xnb",
        };

        private Dictionary<string, string> FontBackupInfo { get; } = new Dictionary<string, string>();

        private List<string> DuplicateFonts { get; } = new List<string>();

        public bool CopyFonts(string japaneseFontsFolderPath, string gameFontsFolderPath, bool isOverride = true)
        {
            OptionsEntry.GetInstance();
            string empty = string.Empty;
            bool result = false;
            string[] array = TrFontsUtils.fontFiles;
            foreach (string path in array)
            {
                if (this.CopyFont(Path.Combine(japaneseFontsFolderPath, path), Path.Combine(gameFontsFolderPath, path), isOverride))
                {
                    result = true;
                }
            }

            return result;
        }

        /// <summary>
        /// フォントのバックアップ
        /// </summary>
        /// <param name="gameFontsFolderPath">フォントフォルダー</param>
        /// <param name="bkupFontsFolderPath">バックアップフォルダー</param>
        /// <returns>成否</returns>
        public bool BkupFonts(string gameFontsFolderPath, string bkupFontsFolderPath)
        {
            OptionsEntry instance = OptionsEntry.GetInstance();
            this.MakeBkupFontInfo();
            DateTime now = DateTime.Now;
            string strDate = now.ToShortDateString().Replace("/", "-");
            now = DateTime.Now;
            string strTime = now.ToLongTimeString().Replace(":", ".");
            string path = string.Format("{0}_{1}", strDate, strTime);
            string dateFolderPath = Path.Combine(bkupFontsFolderPath, path);
            bool result = false;

            foreach (string fontFileName in TrFontsUtils.fontFiles)
            {
                string fontFilePath = Path.Combine(gameFontsFolderPath, fontFileName);
                string fontHash = FileHash.CalcHash(fontFilePath);
                if (!this.ExistsBackup(fontHash))
                {
                    //// fontHash に一致するファイルがない場合は、バックアップする。
                    string backupFontPath = Path.Combine(instance.TerrariaJapaneseFontsFolderPath, Path.GetFileName(fontFilePath));
                    if (!(FileHash.CalcHash(fontFilePath) == FileHash.CalcHash(backupFontPath)))
                    {
                        TrFileUtils.SafeCreateDirectory(dateFolderPath);
                        var outPath = Path.Combine(dateFolderPath, fontFileName);
                        this.CopyFont(fontFilePath, outPath, true);
                        result = true;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// ファイルをコピーする。
        /// </summary>
        /// <param name="inPath">入力ファイルのパス</param>
        /// <param name="outPath">出力ファイルのパス</param>
        /// <param name="isOverride">上書きの有無</param>
        /// <returns>成否</returns>
        private bool CopyFont(string inPath, string outPath, bool isOverride)
        {
            if (FileHash.CalcHash(inPath) == FileHash.CalcHash(outPath))
            {
                return false;
            }

            File.Copy(inPath, outPath, isOverride);
            return true;
        }

        private void MakeBkupFontInfo()
        {
            OptionsEntry instance = OptionsEntry.GetInstance();
            this.DuplicateFonts.Clear();
            string[] files = Directory.GetFiles(instance.TerrariaBkupFontsFolderPath, "*.xnb", SearchOption.AllDirectories);
            foreach (string text in files)
            {
                if (!this.Add(FileHash.CalcHash(text), text))
                {
                    this.DuplicateFonts.Add(text);
                }
            }
        }

        private bool ExistsBackup(string hash)
        {
            return this.FontBackupInfo.ContainsKey(hash);
        }

        private bool Add(string hash, string fontPath)
        {
            if (this.FontBackupInfo.ContainsKey(hash))
            {
                return false;
            }

            this.FontBackupInfo.Add(hash, fontPath);
            return true;
        }
    }
}

namespace TerrariaJpModLib.Models.TransXml
{
    using System.Collections.Generic;

    public class TrTransXmlData
    {
        public Dictionary<string, Entry> Items { get; } = new Dictionary<string, Entry>();

        public void Add(Entry newEntry)
        {
            string key = $"{newEntry.Category}:{newEntry.English}";
            if (this.Items.ContainsKey(key))
            {
                //// ToDo: 不要なロジック
                bool flag = this.Items[key].Japanese.ToLower() == newEntry.Japanese.ToLower();
            }
            else
            {
                this.Items.Add(key, newEntry);
            }
        }

        public string GetJapaneseByEnglish(string english)
        {
            foreach (Entry value in this.Items.Values)
            {
                if (value.English.ToLower().Trim() == english.ToLower().Trim())
                {
                    return value.Japanese;
                }
            }

            return english;
        }

        public class Entry
        {
            public Entry(string category, string english, string japanese)
            {
                this.Category = category;
                this.English = english;
                this.Japanese = japanese;
            }

            public string Category { get; set; } = string.Empty;

            public string English { get; set; } = string.Empty;

            public string Japanese { get; set; } = string.Empty;
        }
    }
}

namespace TerrariaJpModLib.Models.TransXml
{
    using System;
    using System.Data;
    using System.IO;
    using System.Text;

    [Obsolete("�g�p�֎~")]
    public class TrTransXmlDataDao
    {
        public static TrTransXmlData LoadFromXml(string path)
        {
            TrTransXmlData trTransXmlData = new TrTransXmlData();
            using (StreamReader reader = new StreamReader(path, Encoding.UTF8))
            {
                DataSet dataSet = new DataSet();
                dataSet.ReadXml((TextReader)reader);
                foreach (DataTable table in dataSet.Tables)
                {
                    foreach (DataRow row in table.Rows)
                    {
                        if (row.ItemArray.Length == 4)
                        {
                            row.ItemArray[0].ToString();
                            string english = row.ItemArray[1].ToString();
                            string japanese = row.ItemArray[2].ToString();
                            TrTransXmlData.Entry newEntry = new TrTransXmlData.Entry(table.TableName, english, japanese);
                            trTransXmlData.Add(newEntry);
                        }
                    }
                }

                return trTransXmlData;
            }
        }
    }
}

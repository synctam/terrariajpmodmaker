namespace TerrariaJpModLib.Models.TModLoader
{
    using TrResouceEditLib.ResourceIO;

    public class TModLoaderInfo
    {
        public TModLoaderInfo(string path)
        {
            this.Enabled = this.ContainsTModLoader(path);
        }

        public bool Enabled { get; private set; }

        private bool ContainsTModLoader(string path)
        {
            return TrResourceUtils.HasClass(path, "Terraria.ModLoader.ModLoader");
        }
    }
}

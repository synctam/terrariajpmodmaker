namespace TerrariaJpModLib.Models.Mod
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using TerrariaJpModLib.Models.LocalizeFile;
    using TerrariaJpModLib.Models.TransSheet;
    using TerrariaJpModLib.UntranslationCommand;
    using TerrariaJpModLib.UnTranslationTable;
    using TrResouceEditLib.LaaPatch;
    using TrResouceEditLib.ResourceIO;

    public class TrModMaker
    {
        public static void ModMake(
            TrContentData contentDataE,
            TrTransSheet newSheet,
            string inputResourcePath,
            string outputResourcePath,
            TrUntranslationTableInfo untranslatedTableInfo,
            TrUntranslationCommandInfo untranslatedCommandInfo,
            bool isDebug = false)
        {
            var dictionary = new Dictionary<string, byte[]>();
            new UTF8Encoding(false);
            foreach (var langFile in contentDataE.Items)
            {
                var buff = new StringBuilder();
                buff.AppendLine("{");
                var fileKey = langFile.Key;
                var fileArg = string.Empty;
                foreach (var langGroup in langFile.Value.Items.Values)
                {
                    var groupKey = langGroup.GroupName;
                    var groupArg = string.Empty;
                    buff.AppendLine(string.Format("\t{0}\"{1}\": ", fileArg, groupKey) + "{");
                    foreach (var langEntry in langGroup.Items.Values)
                    {
                        var entryKey = langEntry.Key;
                        var text = newSheet.GetJapaneseFromKey(fileKey, groupKey, entryKey, langEntry.Text);
                        if (string.IsNullOrEmpty(text))
                        {
                            text = langEntry.Text;
                        }
                        else
                        {
                            if (!untranslatedTableInfo.IsTranslation(langFile.Key, langGroup.GroupName))
                            {
                                //// UnTranslationTable により翻訳除外
                                text = langEntry.Text;
                            }

                            if (!untranslatedCommandInfo.IsTranslation(langFile.Key, langGroup.GroupName))
                            {
                                //// UnTranslationCommand により翻訳除外
                                text = langEntry.Text;
                            }
                        }

                        text = TrModMaker.EscapeForJson(text);
                        var value = string.Format("\t\t{0}\"{1}\": \"{2}\"", groupArg, entryKey, text);
                        buff.AppendLine(value);
                        groupArg = ",";
                    }

                    fileArg = ",";
                    buff.AppendLine("\t}");
                }

                buff.AppendLine("}");
                buff.ToString();
                if (isDebug)
                {
                    Console.WriteLine(string.Format("検証用JSONファイル出力({0}.json)...", fileKey));
                    using (StreamWriter streamWriter = new StreamWriter("data\\" + fileKey + ".json", false, Encoding.UTF8))
                    {
                        streamWriter.Write(buff.ToString());
                    }
                }

                byte[] value2 = TrResourceUtils.StringToArray(buff.ToString(), null);
                dictionary.Add(fileKey + ".json", value2);
            }

            TrResourceUtils.ReplaceResourceFromResourceList(inputResourcePath, outputResourcePath, dictionary);
            new LaaPatch(outputResourcePath).SetLaa();
        }

        private static string EscapeForJson(string text)
        {
            StringBuilder buff = new StringBuilder(text);
            buff.Replace("\"", "\\\"");
            buff.Replace("\\", "\\");
            buff.Replace("\b", "\\b");
            buff.Replace("\f", "\\f");
            buff.Replace("\n", "\\n");
            buff.Replace("\r", "\\r");
            buff.Replace("\t", "\\t");
            return buff.ToString();
        }
    }
}

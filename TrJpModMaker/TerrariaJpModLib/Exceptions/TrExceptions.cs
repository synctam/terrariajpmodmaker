namespace TrJpModMaker.Exceptions
{
    using System;

    public class TrExceptions
    {
        public class UnSupportedVersionException : Exception
        {
            public UnSupportedVersionException(string msg)
                : base(msg)
            {
            }
        }

        public class JsonResourceNotFoundException : Exception
        {
            public JsonResourceNotFoundException(string msg)
                : base(msg)
            {
            }
        }

        public class AlreadyRunningException : Exception
        {
            public AlreadyRunningException(string msg)
                : base(msg)
            {
            }
        }
    }
}

namespace TerrariaJpModLib.UntranslationCommand
{
    using System.IO;
    using System.Text;
    using CsvHelper;
    using CsvHelper.Configuration;
    using TerrariaJpModLib.UnTranslationTable;

    public class TrUntranslationCommandDao
    {
        public static void LoadFromCsv(
            TrUntranslationCommandInfo untranslatedCommandInfo, string path, Encoding enc = null)
        {
            var encoding = new UTF8Encoding(false);
            enc = enc ?? encoding;
            using (var reader = new StreamReader(path, enc))
            {
                using (var csvReader = new CsvReader(reader))
                {
                    csvReader.Configuration.Delimiter = ",";
                    csvReader.Configuration.HasHeaderRecord = true;
                    csvReader.Configuration.RegisterClassMap<CsvMapper>();
                    csvReader.Configuration.RegisterClassMap<CsvMapper>();

                    var records = csvReader.GetRecords<TrUntranslationCommandEntry>();
                    foreach (var entry in records)
                    {
                        untranslatedCommandInfo.AddEntry(entry);
                    }
                }
            }
        }

#if WINDOWS_7
        public class CsvMapper : CsvClassMap<TrUntranslationCommandEntry>
#else
        public class CsvMapper : ClassMap<TrUntranslationCommandEntry>
#endif
        {
            public CsvMapper()
            {
                this.Map((x) => x.FileID).Name("[[FileID]]");
                this.Map((x) => x.Group).Name("[[Group]]");
            }
        }
    }
}

namespace TerrariaJpModLib.UntranslationCommand
{
    using System;
    using System.Collections.Generic;

    public class TrUntranslationCommandInfo
    {
        private const char Delimiter = ':';

        /// <summary>
        /// 非翻訳グループの辞書
        /// キーは FileID:Group
        /// </summary>
        public SortedDictionary<string, TrUntranslationCommandEntry> Items { get; } =
            new SortedDictionary<string, TrUntranslationCommandEntry>();

        public void AddEntry(TrUntranslationCommandEntry entry)
        {
            var key = this.MakeKey(entry.FileID, entry.Group);
            if (this.Items.ContainsKey(key))
            {
                //// 既に登録済みの場合は無視する。
                Console.WriteLine($"Duplicate key({key})");
            }
            else
            {
                this.Items.Add(key, entry);
            }
        }

        public bool IsTranslation(string fileID, string group)
        {
            fileID = fileID + ".json";
            var key = this.MakeKey(fileID, group);
            if (this.Items.ContainsKey(key))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private string MakeKey(string fileID, string group)
        {
            return $"{fileID}{Delimiter}{group}";
        }
    }
}

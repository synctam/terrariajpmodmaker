namespace TerrariaJpModLib.UntranslationCommand
{
    public class TrUntranslationCommandEntry
    {
        public string FileID { get; set; } = string.Empty;

        public string Group { get; set; } = string.Empty;
    }
}

namespace TrJpModMaker.Models.HashUtils
{
    using System;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;
    using TrJpModMaker.Exceptions;

    public class FileHash
    {
        public static string CalcHash(string path)
        {
            try
            {
                if (File.Exists(path))
                {
                    using (FileStream inputStream = new FileStream(path, FileMode.Open))
                    {
                        using (SHA256Managed sHA256Managed = new SHA256Managed())
                        {
                            byte[] array = sHA256Managed.ComputeHash(inputStream);
                            sHA256Managed.Clear();
                            StringBuilder buff = new StringBuilder();
                            byte[] array2 = array;
                            for (int i = 0; i < array2.Length; i++)
                            {
                                byte b = array2[i];
                                buff.Append(b.ToString("x2"));
                            }

                            return buff.ToString();
                        }
                    }
                }

                return string.Empty;
            }
            catch (IOException ex)
            {
                throw new TrExceptions.AlreadyRunningException(ex.Message);
            }
        }
    }
}

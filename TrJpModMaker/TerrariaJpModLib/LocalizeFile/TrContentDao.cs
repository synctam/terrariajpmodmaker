namespace TerrariaJpModLib.Models.LocalizeFile
{
    using System;
    using System.IO;
    using System.Text;
    using Newtonsoft.Json;
    using TerrariaJpModLib.Models.Resource;
    using TrResouceEditLib.ResourceIO;

    public class TrContentDao
    {
        public static TrContentData LoadFromFolder(string folderPath)
        {
            TrContentData trContentData = new TrContentData();
            string[] files = Directory.GetFiles(folderPath, "*.json", SearchOption.AllDirectories);
            foreach (string path in files)
            {
                trContentData.Add(LoadFromJson(path, null));
            }

            return trContentData;
        }

        public static TrContentData LoadFromResource(string exePath, bool istModLoader = false)
        {
            var instance = TrResource.GetInstance();
            instance.IsTModLoader = istModLoader;
            var trContentData = new TrContentData();
            foreach (string item in instance.Items)
            {
                var resource = TrResourceUtils.GetResource(exePath, item);
                trContentData.Add(LoadFromStream(resource, Path.GetFileNameWithoutExtension(item), null));
            }

            return trContentData;
        }

        public static TrContentData.File LoadFromJson(string path, Encoding enc = null)
        {
            Encoding encoding = new UTF8Encoding(false);
            enc = enc ?? encoding;
            using (StreamReader streamReader = new StreamReader(path, enc))
            {
                string s = streamReader.ReadToEnd();
                using (Stream stream = new MemoryStream(enc.GetBytes(s)))
                {
                    return LoadFromStream(stream, Path.GetFileNameWithoutExtension(path), enc);
                }
            }
        }

        public static TrContentData.File LoadFromStream(Stream stream, string fileID, Encoding enc = null)
        {
            Encoding encoding = new UTF8Encoding(false);
            TrContentData.File file = new TrContentData.File(fileID);
            enc = enc ?? encoding;
            using (StreamReader reader = new StreamReader(stream, enc))
            {
                JsonTextReader jsonTextReader = new JsonTextReader(reader);
                string empty2 = string.Empty;
                string key = string.Empty;
                string empty = string.Empty;
                TrContentData.Group group = null;
                while (jsonTextReader.Read())
                {
                    if (jsonTextReader.Depth == 1 && jsonTextReader.Value != null)
                    {
                        group = new TrContentData.Group(jsonTextReader.Value.ToString());
                        group.Parent = file;
                        file.Add(group);
                    }
                    else if (jsonTextReader.Depth == 2 && jsonTextReader.Value != null)
                    {
                        switch (jsonTextReader.TokenType)
                        {
                            case JsonToken.PropertyName:
                                key = jsonTextReader.Value.ToString();
                                break;
                            case JsonToken.String:
                                {
                                    empty = jsonTextReader.Value.ToString();
                                    TrContentData.Entry entry = new TrContentData.Entry(key, empty);
                                    entry.Parent = group;
                                    group.Add(entry);
                                    break;
                                }

                            default:
                                throw new Exception(string.Format("TokenType Error: {0}", jsonTextReader.TokenType));
                            case JsonToken.Comment:
                                break;
                        }
                    }
                }

                return file;
            }
        }

        public static void WriteJsonFileFromExe(string exePath, string outFolderPath)
        {
            TrResource instance = TrResource.GetInstance();
            new TrContentData();
            foreach (string item in instance.Items)
            {
                MemoryStream resource = TrResourceUtils.GetResource(exePath, item);
                using (StreamWriter streamWriter = new StreamWriter(Path.Combine(outFolderPath, item), false, Encoding.UTF8))
                {
                    streamWriter.Write(TrResourceUtils.StreamToString(resource, null));
                }
            }
        }
    }
}

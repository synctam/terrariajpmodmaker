namespace TerrariaJpModLib.Models.LocalizeFile
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class TrContentData
    {
        public Dictionary<string, File> Items { get; } = new Dictionary<string, File>();

        public void Add(File file)
        {
            this.Items.Add(file.FileID, file);
        }

        public override string ToString()
        {
            var buff = new StringBuilder();

            foreach (var file in this.Items.Values)
            {
                buff.Append(file.ToString());
            }

            return buff.ToString();
        }

        public class File
        {
            public File(string fileID) { this.FileID = fileID; }

            private File() { }

            public Dictionary<string, Group> Items { get; } = new Dictionary<string, Group>();

            public string FileID { get; } = string.Empty;

            public void Add(Group group)
            {
                this.Items.Add(group.GroupName, group);
            }

            public override string ToString()
            {
                var buff = new StringBuilder();

                buff.AppendLine($"FileID({this.FileID})");
                foreach (var group in this.Items.Values)
                {
                    buff.Append(group.ToString());
                }

                return buff.ToString();
            }
        }

        public class Group
        {
            public Group(string groupName)
            {
                this.GroupName = groupName;
            }

            private Group() { }

            public Dictionary<string, Entry> Items { get; } = new Dictionary<string, Entry>();

            public string GroupName { get; } = string.Empty;

            public File Parent { get; set; }

            public void Add(Entry entry)
            {
                if (this.Items.ContainsKey(entry.Key))
                {
                    //// すでに存在する場合は上書きする。
                    var currentEntry = this.Items[entry.Key];
                    currentEntry.ReplaceText(entry.Text);
                }
                else
                {
                    this.Items.Add(entry.Key, entry);
                }
            }

            public override string ToString()
            {
                var buff = new StringBuilder();

                buff.AppendLine($"\tFileID({this.GroupName})");
                foreach (var entry in this.Items.Values)
                {
                    buff.Append(entry.ToString());
                }

                return buff.ToString();
            }
        }

        public class Entry
        {
            public Entry(string key, string text)
            {
                this.Key = key;
                this.Text = text;
            }

            private Entry() { }

            public string Key { get; } = string.Empty;

            public string Text { get; private set; } = string.Empty;

            public Group Parent { get; set; }

            public override string ToString()
            {
                var buff = new StringBuilder();

                buff.AppendLine($"\t\tKey({this.Key}) Text({this.Text})");

                return buff.ToString();
            }

            internal void ReplaceText(string text)
            {
                if (this.Text.Equals(text))
                {
                    //// 変更がない場合は無視。
                }
                else
                {
                    this.Text = text;
                }
            }
        }
    }
}

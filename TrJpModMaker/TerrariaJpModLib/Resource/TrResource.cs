namespace TerrariaJpModLib.Models.Resource
{
    using System.Collections.Generic;

    public class TrResource
    {
        private static TrResource instance = new TrResource();
        private bool isTModLoader = false;

        private TrResource()
        {
            this.SetJsonResourceNames(false);
        }

        public List<string> Items { get; } = new List<string>();

        public bool IsTModLoader
        {
            get
            {
                return this.isTModLoader;
            }

            set
            {
                this.SetJsonResourceNames(value);
                this.isTModLoader = value;
            }
        }

        public static TrResource GetInstance()
        {
            return instance;
        }

        public void SetJsonResourceNames(bool isTModLoader = false)
        {
            this.Items.Clear();
            this.Items.Add("Terraria.Localization.Content.en-US.Game.json");
            this.Items.Add("Terraria.Localization.Content.en-US.Items.json");
            if (isTModLoader)
            {
                this.Items.Add("Terraria.Localization.Content.en-US.Main.json");
                this.Items.Add("Terraria.Localization.Content.en-US.tModLoader.json");
            }
            else
            {
                this.Items.Add("Terraria.Localization.Content.en-US.json");
            }

            this.Items.Add("Terraria.Localization.Content.en-US.Legacy.json");
            this.Items.Add("Terraria.Localization.Content.en-US.NPCs.json");
            this.Items.Add("Terraria.Localization.Content.en-US.Projectiles.json");
            this.Items.Add("Terraria.Localization.Content.en-US.Town.json");
        }
    }
}

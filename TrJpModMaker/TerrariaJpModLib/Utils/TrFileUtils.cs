#pragma warning disable SA1008 // Opening parenthesis must be spaced correctly
namespace TerrariaJpModLib.Utils
{
    using System.IO;

    public class TrFileUtils
    {
        private const char Delimiter = '#';

        /// <summary>
        /// 指定したパスにディレクトリが存在しない場合
        /// すべてのディレクトリとサブディレクトリを作成します
        /// </summary>
        /// <param name="path">Path</param>
        /// <returns>DirectoryInfo</returns>
        public static DirectoryInfo SafeCreateDirectory(string path)
        {
            if (Directory.Exists(path))
            {
                return null;
            }

            return Directory.CreateDirectory(path);
        }

        public static string ConvertParaTransKey(string fileID, string group, string key)
        {
            return $"{fileID}{Delimiter}{group}{Delimiter}{key}";
        }

        public static string GetFileID(string text)
        {
            string[] param = text.Split(Delimiter);
            var fileID = param[0];
            return fileID;
        }

        public static string GetGroup(string text)
        {
            string[] param = text.Split(Delimiter);
            var group = param[1];
            return group;
        }

        public static string GetKey(string text)
        {
            string[] param = text.Split(Delimiter);
            var key = param[2];
            return key;
        }
    }
}
#pragma warning restore SA1008 // Opening parenthesis must be spaced correctly

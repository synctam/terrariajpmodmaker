namespace TerrariaJpModLib.Models.Translation
{
    using System.Text.RegularExpressions;
    using TerrariaJpModLib.Models.TransSheet;
    using TerrariaJpModLib.Models.TransXml;

    public class TrTranslation
    {
        public static TrTransSheet Translate(TrTransSheet oldTransSheet, TrTransXmlData xmlData, bool full = false)
        {
            TrTransSheet trTransSheet = new TrTransSheet();
            foreach (TrTransSheet.SheetEntry value in oldTransSheet.Items.Values)
            {
                if (NeedTranslation(value.English) || full)
                {
                    TrTransSheet.SheetEntry sheetEntry = value.Clone();
                    string japaneseByEnglish = xmlData.GetJapaneseByEnglish(value.English);
                    if (!(value.English.ToLower().Trim() == japaneseByEnglish.ToLower().Trim()))
                    {
                        sheetEntry.Japanese = japaneseByEnglish;
                    }

                    trTransSheet.AddEntry(sheetEntry);
                }
            }

            return trTransSheet;
        }

        private static bool NeedTranslation(string text)
        {
            foreach (Match item in Regex.Matches(text, "\\{.*?\\}"))
            {
                text = text.Replace(item.Value, string.Empty);
            }

            if (string.IsNullOrEmpty(text))
            {
                return false;
            }

            return true;
        }
    }
}

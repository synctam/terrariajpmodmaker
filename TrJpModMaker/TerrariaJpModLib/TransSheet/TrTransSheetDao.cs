namespace TerrariaJpModLib.Models.TransSheet
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using CsvHelper;
    using CsvHelper.Configuration;
    using TerrariaJpModLib.Models.LocalizeFile;
    using TerrariaJpModLib.Utils;

    public class TrTransSheetDao
    {
        public static TrTransSheet LoadFromCsv(string path, Encoding enc = null)
        {
            Encoding encoding = new UTF8Encoding(false);
            enc = enc ?? encoding;
            using (var reader = new StreamReader(path, enc))
            {
                using (var csvReader = new CsvReader(reader))
                {
                    var log = new StringBuilder();

                    csvReader.Configuration.Delimiter = ",";
                    csvReader.Configuration.HasHeaderRecord = true;
                    csvReader.Configuration.RegisterClassMap<CsvMapper>();
                    csvReader.Configuration.RegisterClassMap<CsvMapper>();
                    var records = csvReader.GetRecords<TrTransSheet.SheetEntry>();
                    var trTransSheet = new TrTransSheet();
                    foreach (TrTransSheet.SheetEntry item in records)
                    {
                        item.IsInvalidText = IsInvalidCtrlCodeString(item.Japanese);
                        if (item.IsInvalidText)
                        {
                            log.AppendLine($"Invalid control char. FileID({item.FileID}) Key({item.Key})");
                            log.AppendLine($"\tOriginal text:   {item.English}");
                            log.AppendLine($"\tTranslated text: {item.Japanese}");
                        }

                        trTransSheet.AddEntry(item);
                    }

                    if (log.Length > 0)
                    {
                        File.WriteAllText(@"TrJpModMaker_log.txt", log.ToString(), Encoding.UTF8);
                    }

                    return trTransSheet;
                }
            }
        }

        public static TrTransSheet MakeTransSheetFromContentData(TrContentData jsonData)
        {
            TrTransSheet trTransSheet = new TrTransSheet();
            foreach (TrContentData.File value in jsonData.Items.Values)
            {
                int num = 1;
                foreach (TrContentData.Group value2 in value.Items.Values)
                {
                    int num2 = 1;
                    foreach (TrContentData.Entry value3 in value2.Items.Values)
                    {
                        TrTransSheet.SheetEntry sheetEntry = new TrTransSheet.SheetEntry();
                        sheetEntry.English = value3.Text;
                        sheetEntry.FileID = value.FileID;
                        sheetEntry.Group = value2.GroupName;
                        sheetEntry.GroupNo = num;
                        sheetEntry.Japanese = string.Empty;
                        sheetEntry.Key = value3.Key;
                        sheetEntry.MTrans = string.Empty;
                        sheetEntry.EntryNo = num2;
                        trTransSheet.AddEntry(sheetEntry);
                        num2++;
                    }

                    num++;
                }
            }

            return trTransSheet;
        }

        public static bool IsInvalidCtrlCodeString(string text)
        {
            bool result = false;
            if (text.Contains("\\"))
            {
                bool isCtrlMode = false;
                foreach (var c in text)
                {
                    if (isCtrlMode)
                    {
                        //// 制御文字状態
                        if (c == 'n')
                        {
                            //// 通常文字状態に変更
                            isCtrlMode = false;
                        }
                        else
                        {
                            //// \ の次に不正な文字がある
                            result = true;
                        }
                    }
                    else
                    {
                        //// 通常文字状態
                        if (c == '\\')
                        {
                            //// 制御文字状態に変更
                            isCtrlMode = true;
                        }
                        else
                        {
                            //// 通常文字
                        }
                    }
                }
            }
            else
            {
                //// 制御文字が含まれていない。
            }

            return result;
        }

#if WINDOWS_7
        public class CsvMapper : CsvClassMap<TrTransSheet.SheetEntry>
#else
        public class CsvMapper : ClassMap<TrTransSheet.SheetEntry>
#endif
        {
            public CsvMapper()
            {
                this.Map((x) => x.FileID).Name("[[FileID]]");
                this.Map((x) => x.Group).Name("[[Group]]");
                this.Map((x) => x.Key).Name("[[Key]]");
                this.Map((x) => x.English).Name("[[English]]");
                this.Map((x) => x.Japanese).Name("[[Japanese]]");
            }
        }
    }
}

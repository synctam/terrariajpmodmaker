namespace TerrariaJpModLib.Models.TransSheet
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using TerrariaJpModLib.Utils;

    public class TrTransSheet
    {
        public Dictionary<string, SheetEntry> Items { get; } = new Dictionary<string, SheetEntry>();

        public void AddEntry(SheetEntry entry)
        {
            string key = TrFileUtils.ConvertParaTransKey(entry.FileID, entry.Group, entry.Key);
            if (this.Items.ContainsKey(key))
            {
                StringBuilder buff = new StringBuilder();
                buff.AppendLine("日本語化MODの作成に失敗しました。");
                buff.AppendLine("翻訳シートのキーが重複しています。");
                buff.AppendLine($"FileID({entry.FileID})");
                buff.AppendLine($"Group({entry.Group})");
                buff.AppendLine($"Key({entry.Key})");
                throw new ArgumentException(buff.ToString());
            }
            else
            {
                this.Items.Add(key, entry);
            }
        }

        public string GetJapaneseFromKey(string fileID, string group, string key, string english)
        {
            if (fileID == "Terraria.Localization.Content.en-US.Main")
            {
                fileID = "Terraria.Localization.Content.en-US";
            }

            string key2 = TrFileUtils.ConvertParaTransKey(fileID, group, key);
            if (this.Items.ContainsKey(key2))
            {
                return this.Items[key2].Translate(english, false);
            }

            return string.Empty;
        }

        public class SheetEntry
        {
            public string FileID { get; set; }

            public string Group { get; set; }

            public int GroupNo { get; set; }

            public string Key { get; set; }

            public string English { get; set; }

            public string Japanese { get; set; }

            public string MTrans { get; set; }

            public int EntryNo { get; set; }

            /// <summary>
            /// テキストに誤り（改行文字）が含まれるかどうか。
            /// </summary>
            public bool IsInvalidText { get; set; } = false;

            public string Translate(string english, bool useMtrans = false)
            {
                string empty = string.Empty;
                bool enEmpty = string.IsNullOrEmpty(english);
                bool jpEmpty = string.IsNullOrEmpty(this.Japanese);
                bool mEmpty = string.IsNullOrEmpty(this.MTrans);
                if (enEmpty)
                {
                    return english;
                }

                if (jpEmpty && mEmpty)
                {
                    return english;
                }

                if (!jpEmpty && mEmpty)
                {
                    return this.Japanese;
                }

                if (jpEmpty && !mEmpty)
                {
                    if (useMtrans)
                    {
                        return this.MTrans;
                    }

                    return english;
                }

                if (!jpEmpty && !mEmpty)
                {
                    return this.Japanese;
                }

                throw new Exception("想定外のエラー");
            }

            public SheetEntry Clone()
            {
                return new SheetEntry
                {
                    English = this.English,
                    EntryNo = this.EntryNo,
                    FileID = this.FileID,
                    Group = this.Group,
                    GroupNo = this.GroupNo,
                    Japanese = this.Japanese,
                    Key = this.Key,
                    MTrans = this.MTrans
                };
            }
        }

        public class SheetEntryCompare : SheetEntry
        {
            public string JapaneseTarget { get; set; }

            public string CompareResult { get; set; }

            public new SheetEntry Clone()
            {
                return new SheetEntryCompare
                {
                    English = this.English,
                    EntryNo = this.EntryNo,
                    FileID = this.FileID,
                    Group = this.Group,
                    GroupNo = this.GroupNo,
                    Japanese = this.Japanese,
                    JapaneseTarget = this.JapaneseTarget,
                    Key = this.Key,
                    MTrans = this.MTrans,
                    CompareResult = this.CompareResult
                };
            }
        }
    }
}

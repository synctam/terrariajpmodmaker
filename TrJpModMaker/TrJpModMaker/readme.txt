﻿(2021.08.15)

Terraria JpMod Maker(TMM) v1.4.1.0 - Windows 8.1, Windows 10 用
(tModLoaderには対応していません)

これは Terraria v1.4 (Terraria: Journey's End) の日本語化MODを作成するツールです。

このツールには次の機能があります。
・日本語化された Terraria_jp.exe の作成（アイテム名などの各種名称を日本語化しないオプションもあります）
・日本語フォントの登録
・日本語化された Terraria_jp.exe の実行

使い方など詳細は以下の記事を参照願います。
「synctam: Terrariaの日本語化について　その２」
https://synctam.blogspot.jp/2017/05/terraria_20.html


■ライセンス
1.Terraria JpMod Maker(TMM)
This software is released under the MIT License.

The MIT License (MIT) | Open Source Initiative
https://opensource.org/licenses/mit-license.php
licenses/MIT_license - Open Source Group Japan Wiki - Open Source Group Japan - OSDN
https://osdn.jp/projects/opensource/wiki/licenses%2FMIT_license

2. 日本語フォント
「ペン字版 Y.OzFont Ver.14.04」を元に、文字種をShift-JISに限定し、XNB形式のフォントを作成しました。

「ペン字版 Y.OzFont TTCパック - おすすめフォント」
http://yozvox.web.fc2.com/82A882B782B782DF8374834883938367.html

Copyright (c) 2016-08-19, Y.Oz (Y.OzVox) (http://yozvox.web.fc2.com),
with Reserved Font Name "Y.OzFont", "YOzFont", "Y.Oz" and "YOz".

This Font Software is licensed under the SIL Open Font License, Version 1.1.
This license is copied below, and is also available with a FAQ at:
http://scripts.sil.org/OFL

3.日本語化データ
CC BY-NC-SA 4.0（表示 - 非営利 - 継承 4.0 国際）


■謝辞
Terraria v1.4 対応版
Terraria v1.4版の有志翻訳は、以下の方々他１２名のご協力により実現できました。
（アルファベット順）
・482F 様
・Mr.ケーケー 様
・umaaji298 様
・ねこぜ 様
・りり 様
・底辺こども部屋おじさん 様
ご協力ありがとうございました。

Terraria v1.3.5 対応版
Terraria JpMod Makerでは「Terraria日本語化プロジェクト Wiki*」2017/04/17版の日本語XMLデータから翻訳シートを作成し使用しています。Wikiの管理人さん、有志翻訳プロジェクトの皆さんに感謝いたします。


■履歴
2021.08.15  翻訳の調整を行いました。
2020.08.23	Terraria v1.4 に対応しました。
2020.06.07	リリース候補１（限定公開）。
			チャットコマンドを翻訳しないオプションを追加しました。
2020.05.24  Terraria 1.4 Terraria: Journey's End に対応しました。
			新しく追加された項目については英語表記となります。
2018.07.14  tModLoader v0.10.1.5 対応。
            ・翻訳の調整。
            ・Bugfix 翻訳シートにキーの重複があった場合、重複箇所を表示するように修正した。
2018.03.01	■TMM関連
            変更なし
            ■翻訳シート
            ・tModLoader v0.10.1.3対応。
2017.12.02	■TMM関連
            ・tModLoader v0.10.1.1対応。
            ■翻訳シート
            ・tModLoader v0.10.1.1対応。
            ・翻訳の調整。
2017.09.08	非公開。v1.2.0.0
            ■TMM関連
            ・tModLoader v0.10.1対応。
            ■翻訳シート
            ・tModLoaderの設定画面の日本語化。
            ・誤字脱字の修正と翻訳の調整。
            ・ゲームパッドの使用方法表示のオン/オフの訳が逆になっていたのを修正した。
            ・フレームスキップのオン/オフの訳が逆になっていたのを修正した。
2017.06.24	v1.1.1.0(1.06.1)
            Bugfix: Out Of Memory対策。Terraria_jp.exeにLAA(Large Address Aware)フラグをセットしメモリー不足が発生しないように修正した。
            翻訳シートの未翻訳部分を少し翻訳および誤訳の修正。
2017.06.11	ｖ1.1.0.0(1.06.0)
            Add： tModLoader対応。
            Bugfix: Terraria.exe変更時、変更を認識していなかった問題を修正し、MODの再作成を促すメッセージを表示するように修正した。
            Changed: 未翻訳箇所の原文を翻訳シートからではなく、JSONファイルから引用するよう変更した。
            Changed: 翻訳シート読み込み時に不要な列を読み込まないように修正した。
            Changed: 中国語以外の表示できなかった文字をフォントに追加した(Latin Characters,Extra Symbols,Cyrillic)。
            Add: Shiftキーを押しながら「Create MOD」ボタンを押すと、検証用JSONファイルを出力する機能を追加した。
2017.05.26	初版公開

以上

#pragma warning disable SA1300 // Element must begin with upper-case letter
namespace TrJpModMaker
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Reflection;
    using System.Text;
    using System.Windows.Forms;
    using TerrariaJpModLib.Models.LocalizeFile;
    using TerrariaJpModLib.Models.Mod;
    using TerrariaJpModLib.Models.Resource;
    using TerrariaJpModLib.Models.TModLoader;
    using TerrariaJpModLib.Models.TransSheet;
    using TerrariaJpModLib.UntranslationCommand;
    using TerrariaJpModLib.UnTranslationTable;
    using TrJpModMaker.Exceptions;
    using TrJpModMaker.Models.Fonts;
    using TrJpModMaker.Models.Options;
    using TrJpModMaker.Properties;
    using TrResouceEditLib.ResourceIO;

    public partial class MainForm : Form
    {
        private FileSystemWatcher terrarisGameFolderWatcher;

        /// <summary>
        /// コンストラクタ：メインフォーム
        /// </summary>
        public MainForm()
        {
            this.InitializeComponent();
            this.AppInit();
        }

        /// <summary>
        /// フォームの初期化
        /// </summary>
        private void AppInit()
        {
            this.Height = 700;
            this.btnCreateMod.Enabled = false;
            this.btnChangeGameFolder.Enabled = true;
            this.btnCopyJapaneseFonts.Enabled = true;

            StringBuilder buff = new StringBuilder();
            buff.AppendLine(
                "■使い方\r\n" +
                "ゲームフォルダーとは Terraria がインストールされているフォルダーのことです。\r\n" +
                "\r\n" +
                "１．TerrariaのEXEの選択\r\n" +
                "「Change Game Folder」ボタンを押し、ゲームフォルダーにある Terraria.exe を選択してください。\r\n" +
                "\r\n" +
                "２．日本語フォントの登録\r\n" +
                "「Copy Japanese Fonts」ボタンを押し、 日本語フォントを登録してください。 " +
                "必要な場合は自動的にフォントのバックアップが取得されます。\r\n" +
                "\r\n" +
                "３．日本語化MODの作成\r\n" +
                "「Create MOD」ボタンを押すと、日本語化された Terraria_jp.exe がゲームフォルダーに作成されます。" +
                "また、チェックボックス「アイテム名などを日本語化しない。」および" +
                "「チャットコマンドを日本語化しない。」にチェックを入れるとそれぞれの項目が英語表記になります。\r\n" +
                "\r\n" +
                "４．ゲームの実行\r\n" +
                "「Run」ボタンを押すと日本語化された Terraria が起動します。\r\n" +
                "Terraria の [Settings]-[Language]で「English > 日本語」を選択してください。\r\n" +
                "\r\n" +
                "５．Terraria アップデート時の操作\r\n" +
                "Terrariaがアップデートされた場合は、「日本語フォントの登録(バックアップも)」と" +
                "「日本語化MODの作成」を再度行ってください。\r\n" +
                "\r\n" +
                "なお、アップデートの内容によっては日本語化MODを作成できない場合もあります、" +
                "その場合は日本語化MODの使用を取りやめ英語版 Terraria をプレイしてください。");
            buff.Append(string.Empty);
            this.txtHelp.AppendText(buff.ToString());

            FileVersionInfo versionInfo = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location);
#if WINDOWS_7
            this.Text = string.Format("Terraria JpMod Maker(Win7) v{0}", versionInfo.FileVersion);
#else
            this.Text = string.Format("Terraria JpMod Maker v{0}", versionInfo.FileVersion);
#endif
            this.toolTip1.SetToolTip(this.btnChangeGameFolder, "ゲームフォルダーを設定します。");
            this.toolTip1.SetToolTip(this.btnCopyJapaneseFonts, "日本語フォントを登録します。");
            this.toolTip1.SetToolTip(this.btnCreateMod, "日本語化MODを作成します。");
            this.toolTip1.SetToolTip(this.btnTestRun, "日本語化されたTerrariaを起動します。\r\nこのフォームは最小化されシステムアイコンに格納されます。");
            this.toolTip1.SetToolTip(this.pictTerrariaIcon, "ゲームフォルダーが設定されている場合はTerrariaのアイコンが表示されます。\r\nまた、ここをクリックするとゲームフォルダーを開きます。");
            this.toolTip1.SetToolTip(this.lvlVersion, "Terrariaのバージョン。");
            this.toolTip1.SetToolTip(this.lvlTModLoader, "tModLoaderを使用しています。");
            this.lvlTModLoader.Text = string.Empty;
            this.lvlVersion.Text = string.Empty;

            this.chkUseTranslationTable.Text = "アイテム名などを日本語化しない。";
            this.toolTip1.SetToolTip(
                this.chkUseTranslationTable,
                @"この設定を変更した場合は、「Create MOD」で日本語化MODを再作成してください。");

            this.chkUseTranslationCommand.Text = "チャットコマンドを日本語化しない。";
            this.toolTip1.SetToolTip(
                this.chkUseTranslationCommand,
                @"この設定を変更した場合は、「Create MOD」で日本語化MODを再作成してください。");

            this.Icon = Resources.TrJpModMaker;
            this.notifyIcon.Icon = Resources.TrJpModMaker;
        }

        /// <summary>
        /// 日本語化MODの作成
        /// </summary>
        /// <param name="isDebug">Debug mode</param>
        /// <returns>日本語化MOD作成の成否</returns>
        private bool MakeMod(bool isDebug)
        {
            var instance = OptionsEntry.GetInstance();
            if (!File.Exists(instance.TransSheetPath))
            {
                MessageBox.Show("翻訳シートが見つかりません。", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return false;
            }

            if (instance.State != OptionsEntry.NOptionsStatus.Loaded)
            {
                return false;
            }

            bool result = false;
            this.Cursor = Cursors.WaitCursor;
            try
            {
                string terrariaExePath = instance.TerrariaExePath;
                string outputResourcePath = Path.Combine(Path.GetDirectoryName(instance.TerrariaExePath), "Terraria_jp.exe");
                TrTransSheet newSheet = TrTransSheetDao.LoadFromCsv(instance.TransSheetPath, null);
                var untranslatedTablePath = string.Empty;

                //// 非翻訳テーブル
                var untranslatedTableInfo = new TrUntranslationTableInfo();
                untranslatedTablePath = @"data\csv\UntranslationTable.csv";
                var untranslatedTableSamplePath = @"data\csv\UntranslationTable(sample).csv";
                if (!File.Exists(untranslatedTablePath))
                {
                    File.Copy(untranslatedTableSamplePath, untranslatedTablePath, false);
                }

                if (instance.UseUntranslationTable)
                {
                    TrUntranslationTableDao.LoadFromCsv(untranslatedTableInfo, untranslatedTablePath);
                }

                //// 非翻訳コマンド
                var untranslatedCommandInfo = new TrUntranslationCommandInfo();
                var untranslatedCommandPath = @"data\csv\UntranslationCommand.csv";
                var untranslatedCommandSamplePath = @"data\csv\UntranslationCommand(sample).csv";
                if (!File.Exists(untranslatedCommandPath))
                {
                    File.Copy(untranslatedCommandSamplePath, untranslatedCommandPath, false);
                }

                if (instance.UseUntranslationCommand)
                {
                    TrUntranslationCommandDao.LoadFromCsv(untranslatedCommandInfo, untranslatedCommandPath);
                }

                TrModMaker.ModMake(
                    TrContentDao.LoadFromResource(terrariaExePath),
                    newSheet,
                    terrariaExePath,
                    outputResourcePath,
                    untranslatedTableInfo,
                    untranslatedCommandInfo,
                    isDebug);
                result = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                result = false;
            }

            this.Cursor = Cursors.Default;
            return result;
        }

        /// <summary>
        /// Terrariaフォルダー設定ダイアログの表示
        /// </summary>
        /// <returns>Terrariaパス情報</returns>
        private TerrariaPathResult GetTerrariaExePathByDialog()
        {
            OptionsEntry.GetInstance();
            FormExePath formExePath = new FormExePath();
            formExePath.Changed = false;
            string terrarisExePath = formExePath.TerrarisExePath;
            if (formExePath.ShowDialog() == DialogResult.OK)
            {
                return new TerrariaPathResult(formExePath.Changed, formExePath.TerrarisExePath);
            }

            return new TerrariaPathResult(false, string.Empty);
        }

        /// <summary>
        /// Terrariaのバージョンチェック
        /// </summary>
        private void CheckSupportedVersion()
        {
            OptionsEntry instance = OptionsEntry.GetInstance();
            if (!(instance.FileVersion.Substring(0, 3) != Settings.Default.SuportedVersion))
            {
                return;
            }

            StringBuilder buff = new StringBuilder();
            buff.AppendLine($"サポート対象外のTerraria.exeです。Version(v{instance.FileVersion})");
            buff.AppendLine($"現在使用可能なバージョンは '{Settings.Default.SuportedVersion}.x'です。(xは任意)");
            buff.AppendLine();
            buff.AppendLine("正しい Terraria.exe を選択してください。");
            throw new TrExceptions.UnSupportedVersionException(buff.ToString());
        }

        /// <summary>
        /// tModLoader使用チェック
        /// </summary>
        private void CheckTModLoader()
        {
            OptionsEntry instance = OptionsEntry.GetInstance();
            TrResource instance2 = TrResource.GetInstance();
            TModLoaderInfo tModLoaderInfo = new TModLoaderInfo(instance.TerrariaExePath);
            instance2.SetJsonResourceNames(tModLoaderInfo.Enabled);
            if (tModLoaderInfo.Enabled)
            {
                this.lvlTModLoader.Text = "with tModLoader";
            }
            else
            {
                this.lvlTModLoader.Text = string.Empty;
            }
        }

        /// <summary>
        /// JSONリソースのチェック
        /// </summary>
        private void CheckSupportedResource()
        {
            OptionsEntry instance = OptionsEntry.GetInstance();
            foreach (string item in TrResource.GetInstance().Items)
            {
                if (TrResourceUtils.GetResource(instance.TerrariaExePath, item) == null)
                {
                    StringBuilder buff = new StringBuilder();
                    buff.AppendLine("Terraria.exeに日本語化に必要なJSONリソースがありません。");
                    buff.AppendLine($"Resource({item})");
                    buff.AppendLine();
                    buff.AppendLine("正しい Terraria.exe を選択してください。");
                    throw new TrExceptions.JsonResourceNotFoundException(buff.ToString());
                }
            }
        }

        /// <summary>
        /// バージョンの書式化
        /// </summary>
        /// <param name="version">バージョン</param>
        /// <returns>書式化されたバージョン</returns>
        private string GetVersionText(string version)
        {
#if WINDOWS_7
            if (string.IsNullOrEmpty(version))
#else
            if (string.IsNullOrWhiteSpace(version))
#endif
            {
                return string.Empty;
            }

            return $"v{version}";
        }

        /// <summary>
        /// GUIの状態の設定：サポート対象外のバージョンの場合
        /// </summary>
        private void SetGuiWhenUnSupportedVersion()
        {
            this.pictTerrariaIcon.Image = null;
            this.lvlVersion.Text = string.Empty;
            this.btnCreateMod.Enabled = false;
            this.btnTestRun.Enabled = false;
            this.btnChangeGameFolder.Enabled = true;
            this.btnCopyJapaneseFonts.Enabled = false;
        }

        /// <summary>
        /// [Event] Form load
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void MainForm_Load(object sender, EventArgs e)
        {
            var instance = OptionsEntry.GetInstance();
            try
            {
                instance.LoadFromXml();
                try
                {
                    instance.LoadFromTerrariaExePath(instance.TerrariaExePath);
                    this.CheckSupportedVersion();
                    this.CheckTModLoader();
                    this.CheckSupportedResource();
                    this.chkUseTranslationTable.Checked = instance.UseUntranslationTable;
                    this.chkUseTranslationCommand.Checked = instance.UseUntranslationCommand;
                    if (instance.NeedUpdate)
                    {
                        StringBuilder buff = new StringBuilder();
                        buff.AppendLine("Terrariaがアップデートされたようです。");
                        buff.AppendLine("日本語化MODを再作成してください。");
                        MessageBox.Show(buff.ToString(), "情報", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        instance.SaveToXml();
                        this.pictTerrariaIcon.Image = instance.IconImage;
                        this.lvlVersion.Text = this.GetVersionText(instance.FileVersion);
                        this.btnCreateMod.Enabled = true;
                        this.btnTestRun.Enabled = false;
                        this.btnChangeGameFolder.Enabled = true;
                        this.btnCopyJapaneseFonts.Enabled = true;
                        this.StartWatchFolder(instance.TerrariaGameFolderPath);
                        this.btnCreateMod.Select();
                    }
                    else
                    {
                        this.pictTerrariaIcon.Image = instance.IconImage;
                        this.lvlVersion.Text = this.GetVersionText(instance.FileVersion);
                        this.btnCreateMod.Enabled = true;
                        this.btnTestRun.Enabled = instance.ExistsTerrariaJpExe();
                        this.btnChangeGameFolder.Enabled = true;
                        this.btnCopyJapaneseFonts.Enabled = true;
                        this.StartWatchFolder(instance.TerrariaGameFolderPath);
                        this.btnTestRun.Select();
                    }
                }
                catch (TrExceptions.UnSupportedVersionException unSupportedVersion)
                {
                    MessageBox.Show(unSupportedVersion.Message, "エラー", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    this.SetGuiWhenUnSupportedVersion();
                    this.StopWatchFolder();
                }
                catch (TrExceptions.JsonResourceNotFoundException jsonResourceNotFound)
                {
                    MessageBox.Show(jsonResourceNotFound.Message, "エラー", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    this.SetGuiWhenUnSupportedVersion();
                    this.StopWatchFolder();
                }
                catch (FileNotFoundException ex)
                {
                    MessageBox.Show(ex.Message, "エラー", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    instance.SaveToXml();
                    this.pictTerrariaIcon.Image = instance.IconImage;
                    this.lvlVersion.Text = this.GetVersionText(instance.FileVersion);
                    this.btnCreateMod.Enabled = false;
                    this.btnTestRun.Enabled = false;
                    this.btnChangeGameFolder.Enabled = true;
                    this.btnCopyJapaneseFonts.Enabled = false;
                    this.StopWatchFolder();
                }
                catch (OptionsEntry.InvalidOptionsPathException ex2)
                {
                    MessageBox.Show(ex2.Message, "エラー", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    instance.SaveToXml();
                    this.pictTerrariaIcon.Image = instance.IconImage;
                    this.lvlVersion.Text = this.GetVersionText(instance.FileVersion);
                    this.btnCreateMod.Enabled = false;
                    this.btnTestRun.Enabled = false;
                    this.btnChangeGameFolder.Enabled = true;
                    this.btnCopyJapaneseFonts.Enabled = false;
                    this.StopWatchFolder();
                }
            }
            catch (FileNotFoundException)
            {
                TerrariaPathResult terrariaExePathByDialog = this.GetTerrariaExePathByDialog();
#if WINDOWS_7
                if (string.IsNullOrEmpty(terrariaExePathByDialog.Path))
#else
                if (string.IsNullOrWhiteSpace(terrariaExePathByDialog.Path))
#endif
                {
                    this.pictTerrariaIcon.Image = instance.IconImage;
                    this.lvlVersion.Text = this.GetVersionText(instance.FileVersion);
                    this.btnCreateMod.Enabled = false;
                    this.btnTestRun.Enabled = false;
                    this.btnChangeGameFolder.Enabled = true;
                    this.btnCopyJapaneseFonts.Enabled = false;
                    this.StopWatchFolder();
                }
                else if (terrariaExePathByDialog.Changed)
                {
                    try
                    {
                        instance.LoadFromTerrariaExePath(terrariaExePathByDialog.Path);
                        this.CheckSupportedVersion();
                        this.CheckTModLoader();
                        this.CheckSupportedResource();
                        instance.NeedUpdate = false;
                        instance.SaveToXml();
                        this.pictTerrariaIcon.Image = instance.IconImage;
                        this.lvlVersion.Text = this.GetVersionText(instance.FileVersion);
                        this.btnCreateMod.Enabled = true;
                        this.btnTestRun.Enabled = false;
                        this.btnChangeGameFolder.Enabled = true;
                        this.btnCopyJapaneseFonts.Enabled = true;
                        this.chkUseTranslationTable.Checked = instance.UseUntranslationTable;
                        this.chkUseTranslationCommand.Checked = instance.UseUntranslationCommand;
                        this.StartWatchFolder(instance.TerrariaGameFolderPath);
                    }
                    catch (TrExceptions.UnSupportedVersionException unSupportedVersion2)
                    {
                        MessageBox.Show(unSupportedVersion2.Message, "エラー", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        this.SetGuiWhenUnSupportedVersion();
                        this.StopWatchFolder();
                    }
                    catch (TrExceptions.JsonResourceNotFoundException jsonResourceNotFound2)
                    {
                        MessageBox.Show(jsonResourceNotFound2.Message, "エラー", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        this.SetGuiWhenUnSupportedVersion();
                        this.StopWatchFolder();
                    }
                    catch (FileNotFoundException ex3)
                    {
                        MessageBox.Show(ex3.Message, "エラー", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        instance.SaveToXml();
                        this.pictTerrariaIcon.Image = instance.IconImage;
                        this.lvlVersion.Text = this.GetVersionText(instance.FileVersion);
                        this.btnCreateMod.Enabled = false;
                        this.btnTestRun.Enabled = false;
                        this.btnChangeGameFolder.Enabled = true;
                        this.btnCopyJapaneseFonts.Enabled = false;
                        this.StopWatchFolder();
                    }
                    catch (OptionsEntry.InvalidOptionsPathException ex4)
                    {
                        MessageBox.Show(ex4.Message, "エラー", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        instance.SaveToXml();
                        this.pictTerrariaIcon.Image = instance.IconImage;
                        this.lvlVersion.Text = this.GetVersionText(instance.FileVersion);
                        this.btnCreateMod.Enabled = false;
                        this.btnTestRun.Enabled = false;
                        this.btnChangeGameFolder.Enabled = true;
                        this.btnCopyJapaneseFonts.Enabled = false;
                        this.StopWatchFolder();
                    }
                }
            }
        }

        /// <summary>
        /// [Event] フォームクローズ前
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.StopWatchFolder();
            this.notifyIcon.Visible = false;
            e.Cancel = false;
        }

        /// <summary>
        /// [Event] MOD作成ボタン
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void btnCreateMod_Click(object sender, EventArgs e)
        {
            bool isDebug = false;
            if ((ModifierKeys & Keys.Shift) == Keys.Shift)
            {
                isDebug = true;
            }

            if (this.MakeMod(isDebug))
            {
                OptionsEntry instance = OptionsEntry.GetInstance();
                this.btnTestRun.Enabled = instance.ExistsTerrariaJpExe();
                instance.NeedUpdate = false;
                MessageBox.Show("日本語化MOD(Terraria_jp.exe)を作成しました。", "情報", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                instance.SaveToXml();
            }
        }

        /// <summary>
        /// [Event] ゲームフォルダーの設定
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void btnChangeGameFolder_Click(object sender, EventArgs e)
        {
            OptionsEntry instance = OptionsEntry.GetInstance();
            TerrariaPathResult terrariaExePathByDialog = this.GetTerrariaExePathByDialog();
#if WINDOWS_7
            if (!string.IsNullOrEmpty(terrariaExePathByDialog.Path) && terrariaExePathByDialog.Changed)
#else
            if (!string.IsNullOrWhiteSpace(terrariaExePathByDialog.Path) && terrariaExePathByDialog.Changed)
#endif
            {
                try
                {
                    instance.LoadFromTerrariaExePath(terrariaExePathByDialog.Path);
                    this.CheckSupportedVersion();
                    this.CheckTModLoader();
                    this.CheckSupportedResource();
                    instance.SaveToXml();
                    this.pictTerrariaIcon.Image = instance.IconImage;
                    this.lvlVersion.Text = this.GetVersionText(instance.FileVersion);
                    this.btnCreateMod.Enabled = true;
                    this.btnTestRun.Enabled = false;
                    this.btnChangeGameFolder.Enabled = true;
                    this.btnCopyJapaneseFonts.Enabled = true;
                    this.StartWatchFolder(instance.TerrariaGameFolderPath);
                }
                catch (TrExceptions.UnSupportedVersionException unSupportedVersion)
                {
                    MessageBox.Show(unSupportedVersion.Message, "エラー", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    this.SetGuiWhenUnSupportedVersion();
                    this.StopWatchFolder();
                }
                catch (TrExceptions.JsonResourceNotFoundException jsonResourceNotFound)
                {
                    MessageBox.Show(jsonResourceNotFound.Message, "エラー", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    this.SetGuiWhenUnSupportedVersion();
                    this.StopWatchFolder();
                }
                catch (FileNotFoundException ex)
                {
                    MessageBox.Show(ex.Message, "エラー", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    instance.SaveToXml();
                    this.pictTerrariaIcon.Image = instance.IconImage;
                    this.lvlVersion.Text = this.GetVersionText(instance.FileVersion);
                    this.btnCreateMod.Enabled = false;
                    this.btnTestRun.Enabled = false;
                    this.btnChangeGameFolder.Enabled = true;
                    this.btnCopyJapaneseFonts.Enabled = false;
                    this.StopWatchFolder();
                }
                catch (OptionsEntry.InvalidOptionsPathException ex2)
                {
                    MessageBox.Show(ex2.Message, "エラー", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    instance.SaveToXml();
                    this.pictTerrariaIcon.Image = instance.IconImage;
                    this.lvlVersion.Text = this.GetVersionText(instance.FileVersion);
                    this.btnCreateMod.Enabled = false;
                    this.btnTestRun.Enabled = false;
                    this.btnChangeGameFolder.Enabled = true;
                    this.btnCopyJapaneseFonts.Enabled = false;
                    this.StopWatchFolder();
                }
            }
        }

        /// <summary>
        /// [Event] 日本語フォントの登録
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void btnCopyJapaneseFonts_Click(object sender, EventArgs e)
        {
            OptionsEntry instance = OptionsEntry.GetInstance();
            string caption = "日本語フォント登録エラー";
            try
            {
                string terrariaJapaneseFontsFolderPath = instance.TerrariaJapaneseFontsFolderPath;
                string terrariaBkupFontsFolderPath = instance.TerrariaBkupFontsFolderPath;
                string terrariaGameFontsFolderPath = instance.TerrariaGameFontsFolderPath;
                StringBuilder buff = new StringBuilder();
                buff.AppendLine("日本語フォントを登録しますか？");
                buff.AppendLine("(必要な場合は自動的にフォントのバックアップも行います)");
                switch (MessageBox.Show(buff.ToString(), "確認", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button3))
                {
                    case DialogResult.OK:
                        {
                            TrFontsUtils trFontsUtils = new TrFontsUtils();
                            bool backupRC = trFontsUtils.BkupFonts(terrariaGameFontsFolderPath, terrariaBkupFontsFolderPath);
                            bool copyRC = trFontsUtils.CopyFonts(terrariaJapaneseFontsFolderPath, terrariaGameFontsFolderPath, true);
                            StringBuilder stringBuilder2 = new StringBuilder();
                            if (backupRC && copyRC)
                            {
                                stringBuilder2.AppendLine("以下の処理が正常に終了しました。");
                                stringBuilder2.AppendLine("・フォントのバックアップ。");
                                stringBuilder2.AppendLine("・日本語フォントの登録。");
                            }
                            else if (!backupRC && copyRC)
                            {
                                stringBuilder2.AppendLine("日本語フォントの登録が正常に終了しました。");
                            }
                            else if (backupRC && !copyRC)
                            {
                                stringBuilder2.AppendLine("フォントのバックアップ行いました。");
                                stringBuilder2.AppendLine("日本語フォントは既に登録済みのため登録をスキップしました。");
                            }
                            else if (!backupRC && !copyRC)
                            {
                                stringBuilder2.AppendLine("登録処理は不要でした。");
                                stringBuilder2.AppendLine("・日本語フォントはすでに登録済みです。");
                                stringBuilder2.AppendLine("・フォントもバックアップされています。");
                            }

                            MessageBox.Show(stringBuilder2.ToString(), "確認", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                            break;
                        }
                }
            }
            catch (FileNotFoundException ex)
            {
                StringBuilder buff = new StringBuilder();
                buff.AppendLine(ex.Message);
                buff.AppendLine();
                buff.AppendLine("Fontsフォルダーを確認してください。");
                MessageBox.Show(buff.ToString(), caption, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            catch (DirectoryNotFoundException ex2)
            {
                StringBuilder buff = new StringBuilder();
                buff.AppendLine(ex2.Message);
                buff.AppendLine();
                buff.AppendLine("ゲームフォルダのFontsフォルダーを確認してください。");
                MessageBox.Show(buff.ToString(), caption, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            catch (TrExceptions.AlreadyRunningException ex3)
            {
                StringBuilder buff = new StringBuilder();
                buff.AppendLine(ex3.Message);
                buff.AppendLine();
                buff.AppendLine("Terrariaが起動している場合はゲームを終了してください。");
                MessageBox.Show(buff.ToString(), caption, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            catch (UnauthorizedAccessException ex4)
            {
                StringBuilder buff = new StringBuilder();
                buff.AppendLine(ex4.Message);
                buff.AppendLine();
                buff.AppendLine("Terrariaが起動している場合はゲームを終了してください。");
                MessageBox.Show(buff.ToString(), caption, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            catch (Exception ex5)
            {
                StringBuilder buff = new StringBuilder();
                buff.AppendLine(ex5.Message);
                buff.AppendLine();
                buff.AppendLine("想定外のエラーが発生しました。");
                buff.AppendLine("Terraria JpMod Makerを再起動してください。");
                MessageBox.Show(buff.ToString(), caption, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        /// <summary>
        /// [Event] Terrariaの起動
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void btnTestRun_Click(object sender, EventArgs e)
        {
            OptionsEntry instance = OptionsEntry.GetInstance();
            if (instance.State == OptionsEntry.NOptionsStatus.Loaded)
            {
                string text = Path.Combine(Path.GetDirectoryName(instance.TerrariaExePath), "Terraria_jp.exe");
                string directoryName = Path.GetDirectoryName(instance.TerrariaExePath);
                if (File.Exists(text))
                {
                    Process process = new Process();
                    process.StartInfo.FileName = text;
                    process.StartInfo.WorkingDirectory = directoryName;
                    process.SynchronizingObject = this;
                    process.Exited += this.OnTerrariaTerminated;
                    process.EnableRaisingEvents = true;
                    process.Start();
                    this.btnCreateMod.Enabled = false;
                    this.btnTestRun.Enabled = false;
                    this.btnChangeGameFolder.Enabled = false;
                    this.btnCopyJapaneseFonts.Enabled = false;
                    this.WindowState = FormWindowState.Minimized;
                    this.Hide();
                }
            }
        }

        /// <summary>
        /// [Event] 終了
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void mnuExit_Click(object sender, EventArgs e)
        {
            this.StopWatchFolder();
            this.Close();
        }

        /// <summary>
        /// [Event] Terrariaのプロセスが終了した。
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void OnTerrariaTerminated(object sender, EventArgs e)
        {
            OptionsEntry instance = OptionsEntry.GetInstance();
            this.btnCreateMod.Enabled = true;
            this.btnTestRun.Enabled = instance.ExistsTerrariaJpExe();
            this.btnChangeGameFolder.Enabled = true;
            this.btnCopyJapaneseFonts.Enabled = true;
            this.Visible = true;
            this.WindowState = FormWindowState.Normal;
            this.Activate();
            this.btnTestRun.Select();
        }

        /// <summary>
        /// [Event] タスクトレイ
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void notifyIcon1_Click(object sender, EventArgs e)
        {
            MouseButtons button = (e as MouseEventArgs).Button;
            if (button == MouseButtons.Left)
            {
                this.Visible = true;
                this.WindowState = FormWindowState.Normal;
                this.Activate();
            }
        }

        /// <summary>
        /// [Event] Resize
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void MainForm_ClientSizeChanged(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.Hide();
            }
        }

        /// <summary>
        /// [Event] Terraria icon clicked
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void pictTerrariaIcon_Click(object sender, EventArgs e)
        {
            string directoryName = Path.GetDirectoryName(OptionsEntry.GetInstance().TerrariaExePath);
#if WINDOWS_7
            if (!string.IsNullOrEmpty(directoryName) && Directory.Exists(directoryName))
#else
            if (!string.IsNullOrWhiteSpace(directoryName) && Directory.Exists(directoryName))
#endif
            {
                Process.Start(directoryName);
            }
        }

        /// <summary>
        /// Terrariaフォルダーの監視をスタート
        /// </summary>
        /// <param name="folderPath">Terrariaフォルダーのパス</param>
        private void StartWatchFolder(string folderPath)
        {
#if WINDOWS_7
            if (string.IsNullOrEmpty(folderPath) && !Directory.Exists(folderPath))
#else
            if (string.IsNullOrWhiteSpace(folderPath) && !Directory.Exists(folderPath))
#endif
            {
                return;
            }

            if (this.terrarisGameFolderWatcher != null)
            {
                this.StopWatchFolder();
            }

            this.terrarisGameFolderWatcher = new FileSystemWatcher();
            this.terrarisGameFolderWatcher.Path = folderPath;
            this.terrarisGameFolderWatcher.NotifyFilter = NotifyFilters.FileName | NotifyFilters.DirectoryName | NotifyFilters.LastWrite | NotifyFilters.LastAccess;
            this.terrarisGameFolderWatcher.Filter = "*.exe";
            this.terrarisGameFolderWatcher.SynchronizingObject = this;
            this.terrarisGameFolderWatcher.Changed += this.watcher_Changed;
            this.terrarisGameFolderWatcher.Created += this.watcher_Changed;
            this.terrarisGameFolderWatcher.Deleted += this.watcher_Changed;
            this.terrarisGameFolderWatcher.Renamed += this.watcher_Renamed;
            this.terrarisGameFolderWatcher.EnableRaisingEvents = true;
        }

        /// <summary>
        /// Terrariaフォルダーの監視を停止
        /// </summary>
        private void StopWatchFolder()
        {
            if (this.terrarisGameFolderWatcher != null)
            {
                this.terrarisGameFolderWatcher.EnableRaisingEvents = false;
                this.terrarisGameFolderWatcher.Dispose();
                this.terrarisGameFolderWatcher = null;
            }
        }

        /// <summary>
        /// [Event] Terraria.exeが変更された
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="e">e</param>
        private void watcher_Changed(object source, FileSystemEventArgs e)
        {
            OptionsEntry instance = OptionsEntry.GetInstance();
            if (instance.State == OptionsEntry.NOptionsStatus.Loaded)
            {
                if (File.Exists(instance.TerrariaJpExePath))
                {
                    this.btnTestRun.Enabled = true;
                }
                else
                {
                    this.btnTestRun.Enabled = false;
                }
            }
        }

        /// <summary>
        /// [Event] Terraria.exeがリネームされた
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="e">e</param>
        private void watcher_Renamed(object source, RenamedEventArgs e)
        {
            var instance = OptionsEntry.GetInstance();
            if (instance.State == OptionsEntry.NOptionsStatus.Loaded)
            {
                if (File.Exists(instance.TerrariaJpExePath))
                {
                    this.btnTestRun.Enabled = true;
                }
                else
                {
                    this.btnTestRun.Enabled = false;
                }
            }
        }

        private void ChkUseTranslationTable_CheckedChanged(object sender, EventArgs e)
        {
            var instance = OptionsEntry.GetInstance();
            if (instance.State == OptionsEntry.NOptionsStatus.Loaded)
            {
                instance.SaveToXml();
                instance.UseUntranslationTable = this.chkUseTranslationTable.Checked;
            }
        }

        private void ChkUseTranslationCommand_CheckedChanged(object sender, EventArgs e)
        {
            var instance = OptionsEntry.GetInstance();
            if (instance.State == OptionsEntry.NOptionsStatus.Loaded)
            {
                instance.SaveToXml();
                instance.UseUntranslationCommand = this.chkUseTranslationCommand.Checked;
            }
        }

        /// <summary>
        /// Terrariaパス取得オブジェクト
        /// </summary>
        private class TerrariaPathResult
        {
            public TerrariaPathResult(bool changed, string path)
            {
                this.Changed = changed;
                this.Path = path;
            }

            public bool Changed { get; }

            public string Path { get; } = string.Empty;
        }
    }
}
#pragma warning restore SA1300 // Element must begin with upper-case letter

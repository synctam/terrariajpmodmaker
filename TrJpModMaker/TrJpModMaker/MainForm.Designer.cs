namespace TrJpModMaker
{
    partial class MainForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnChangeGameFolder = new System.Windows.Forms.Button();
            this.btnTestRun = new System.Windows.Forms.Button();
            this.btnCreateMod = new System.Windows.Forms.Button();
            this.btnCopyJapaneseFonts = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pictTerrariaIcon = new System.Windows.Forms.PictureBox();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.txtHelp = new System.Windows.Forms.TextBox();
            this.lvlVersion = new System.Windows.Forms.Label();
            this.lvlTModLoader = new System.Windows.Forms.Label();
            this.openTerrariaExeDialog = new System.Windows.Forms.OpenFileDialog();
            this.chkUseTranslationTable = new System.Windows.Forms.CheckBox();
            this.chkUseTranslationCommand = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictTerrariaIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // btnChangeGameFolder
            // 
            this.btnChangeGameFolder.Font = new System.Drawing.Font("MS UI Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnChangeGameFolder.Location = new System.Drawing.Point(12, 96);
            this.btnChangeGameFolder.Name = "btnChangeGameFolder";
            this.btnChangeGameFolder.Size = new System.Drawing.Size(180, 23);
            this.btnChangeGameFolder.TabIndex = 2;
            this.btnChangeGameFolder.Text = "Change Game Folder";
            this.btnChangeGameFolder.UseVisualStyleBackColor = true;
            this.btnChangeGameFolder.Click += new System.EventHandler(this.btnChangeGameFolder_Click);
            // 
            // btnTestRun
            // 
            this.btnTestRun.Font = new System.Drawing.Font("MS UI Gothic", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnTestRun.Location = new System.Drawing.Point(384, 50);
            this.btnTestRun.Name = "btnTestRun";
            this.btnTestRun.Size = new System.Drawing.Size(154, 69);
            this.btnTestRun.TabIndex = 1;
            this.btnTestRun.Text = "Run";
            this.btnTestRun.UseVisualStyleBackColor = true;
            this.btnTestRun.Click += new System.EventHandler(this.btnTestRun_Click);
            // 
            // btnCreateMod
            // 
            this.btnCreateMod.Font = new System.Drawing.Font("MS UI Gothic", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnCreateMod.Location = new System.Drawing.Point(12, 50);
            this.btnCreateMod.Name = "btnCreateMod";
            this.btnCreateMod.Size = new System.Drawing.Size(366, 40);
            this.btnCreateMod.TabIndex = 0;
            this.btnCreateMod.Text = "Create MOD";
            this.btnCreateMod.UseVisualStyleBackColor = true;
            this.btnCreateMod.Click += new System.EventHandler(this.btnCreateMod_Click);
            // 
            // btnCopyJapaneseFonts
            // 
            this.btnCopyJapaneseFonts.Font = new System.Drawing.Font("MS UI Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnCopyJapaneseFonts.Location = new System.Drawing.Point(198, 96);
            this.btnCopyJapaneseFonts.Name = "btnCopyJapaneseFonts";
            this.btnCopyJapaneseFonts.Size = new System.Drawing.Size(180, 23);
            this.btnCopyJapaneseFonts.TabIndex = 3;
            this.btnCopyJapaneseFonts.Text = "Copy Japanese Fonts";
            this.btnCopyJapaneseFonts.UseVisualStyleBackColor = true;
            this.btnCopyJapaneseFonts.Click += new System.EventHandler(this.btnCopyJapaneseFonts_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(50, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(302, 29);
            this.label1.TabIndex = 4;
            this.label1.Text = "Terraria JpMod Maker";
            // 
            // pictTerrariaIcon
            // 
            this.pictTerrariaIcon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictTerrariaIcon.Location = new System.Drawing.Point(12, 12);
            this.pictTerrariaIcon.Name = "pictTerrariaIcon";
            this.pictTerrariaIcon.Size = new System.Drawing.Size(32, 32);
            this.pictTerrariaIcon.TabIndex = 5;
            this.pictTerrariaIcon.TabStop = false;
            this.pictTerrariaIcon.Click += new System.EventHandler(this.pictTerrariaIcon_Click);
            // 
            // notifyIcon
            // 
            this.notifyIcon.Text = "notifyIcon";
            this.notifyIcon.Visible = true;
            this.notifyIcon.Click += new System.EventHandler(this.notifyIcon1_Click);
            // 
            // txtHelp
            // 
            this.txtHelp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtHelp.Font = new System.Drawing.Font("MS UI Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtHelp.Location = new System.Drawing.Point(12, 169);
            this.txtHelp.Multiline = true;
            this.txtHelp.Name = "txtHelp";
            this.txtHelp.ReadOnly = true;
            this.txtHelp.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtHelp.Size = new System.Drawing.Size(526, 415);
            this.txtHelp.TabIndex = 6;
            this.txtHelp.TabStop = false;
            // 
            // lvlVersion
            // 
            this.lvlVersion.AutoSize = true;
            this.lvlVersion.Location = new System.Drawing.Point(382, 26);
            this.lvlVersion.Name = "lvlVersion";
            this.lvlVersion.Size = new System.Drawing.Size(88, 12);
            this.lvlVersion.TabIndex = 7;
            this.lvlVersion.Text = "Terraria Version";
            // 
            // lvlTModLoader
            // 
            this.lvlTModLoader.AutoSize = true;
            this.lvlTModLoader.Font = new System.Drawing.Font("MS UI Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lvlTModLoader.Location = new System.Drawing.Point(381, 9);
            this.lvlTModLoader.Name = "lvlTModLoader";
            this.lvlTModLoader.Size = new System.Drawing.Size(129, 15);
            this.lvlTModLoader.TabIndex = 8;
            this.lvlTModLoader.Text = "tModLoaderVersion";
            this.lvlTModLoader.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // openTerrariaExeDialog
            // 
            this.openTerrariaExeDialog.FileName = "Terraria.exe";
            this.openTerrariaExeDialog.Filter = "EXEファイル(*.exe)|*.exe|すべてのファイル(*.*)|*.*";
            this.openTerrariaExeDialog.InitialDirectory = "c:\\\\";
            this.openTerrariaExeDialog.Title = "開く(Terraria.exe)";
            // 
            // chkUseTranslationTable
            // 
            this.chkUseTranslationTable.AutoSize = true;
            this.chkUseTranslationTable.Location = new System.Drawing.Point(12, 125);
            this.chkUseTranslationTable.Name = "chkUseTranslationTable";
            this.chkUseTranslationTable.Size = new System.Drawing.Size(80, 16);
            this.chkUseTranslationTable.TabIndex = 4;
            this.chkUseTranslationTable.Text = "checkBox1";
            this.chkUseTranslationTable.UseVisualStyleBackColor = true;
            this.chkUseTranslationTable.CheckedChanged += new System.EventHandler(this.ChkUseTranslationTable_CheckedChanged);
            // 
            // chkUseTranslationCommand
            // 
            this.chkUseTranslationCommand.AutoSize = true;
            this.chkUseTranslationCommand.Location = new System.Drawing.Point(12, 147);
            this.chkUseTranslationCommand.Name = "chkUseTranslationCommand";
            this.chkUseTranslationCommand.Size = new System.Drawing.Size(80, 16);
            this.chkUseTranslationCommand.TabIndex = 9;
            this.chkUseTranslationCommand.Text = "checkBox1";
            this.chkUseTranslationCommand.UseVisualStyleBackColor = true;
            this.chkUseTranslationCommand.CheckedChanged += new System.EventHandler(this.ChkUseTranslationCommand_CheckedChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(548, 595);
            this.Controls.Add(this.chkUseTranslationCommand);
            this.Controls.Add(this.chkUseTranslationTable);
            this.Controls.Add(this.lvlTModLoader);
            this.Controls.Add(this.lvlVersion);
            this.Controls.Add(this.txtHelp);
            this.Controls.Add(this.pictTerrariaIcon);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCopyJapaneseFonts);
            this.Controls.Add(this.btnCreateMod);
            this.Controls.Add(this.btnTestRun);
            this.Controls.Add(this.btnChangeGameFolder);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainForm";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ClientSizeChanged += new System.EventHandler(this.MainForm_ClientSizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.pictTerrariaIcon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnChangeGameFolder;
        private System.Windows.Forms.Button btnTestRun;
        private System.Windows.Forms.Button btnCreateMod;
        private System.Windows.Forms.Button btnCopyJapaneseFonts;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictTerrariaIcon;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TextBox txtHelp;
        private System.Windows.Forms.Label lvlVersion;
        private System.Windows.Forms.Label lvlTModLoader;
        private System.Windows.Forms.OpenFileDialog openTerrariaExeDialog;
        private System.Windows.Forms.CheckBox chkUseTranslationTable;
        private System.Windows.Forms.CheckBox chkUseTranslationCommand;
    }
}


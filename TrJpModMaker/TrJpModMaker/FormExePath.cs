namespace TrJpModMaker
{
    using System;
    using System.IO;
    using System.Text;
    using System.Windows.Forms;
    using TrJpModMaker.Models.Options;
    using TrJpModMaker.Properties;

    public partial class FormExePath : Form
    {
        private bool canClose = true;

        public FormExePath()
        {
            this.InitializeComponent();

            this.Icon = Resources.TrJpModMaker;
            var buff = new StringBuilder();
            this.Text = "TerrariaのEXEを選択";
            buff.AppendLine("■使い方\r\nゲームフォルダーとは Terraria がインストールされているフォルダーのことです。\r\n\r\n参照ボタンを押し、ゲームフォルダーにある Terraria.exe を選択し、OKボタンを押してください。");
            this.txtHelp.AppendText(buff.ToString());
        }

        public bool Changed { get; set; }

        public string TerrarisExePath { get; private set; } = string.Empty;

        private void FormExePath_Load(object sender, EventArgs e)
        {
            OptionsEntry instance = OptionsEntry.GetInstance();
            this.lblText.Text = "ゲームフォルダーにある Terraria.exe を指定してください。";
            this.TerrarisExePath = instance.TerrariaExePath;
            this.txtTerrariaExePath.Text = this.TerrarisExePath;
        }

        private void BtnBrowse_Click(object sender, EventArgs e)
        {
            OptionsEntry instance = OptionsEntry.GetInstance();
            this.openTerrariaExeDialog.Title = "開く：Terraria.exe";
            this.openTerrariaExeDialog.FileName = "Terraria.exe";
            if (instance.State == OptionsEntry.NOptionsStatus.Loaded)
            {
                this.openTerrariaExeDialog.InitialDirectory =
                    Path.GetDirectoryName(instance.TerrariaExePath);
            }

            if (this.openTerrariaExeDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    this.txtTerrariaExePath.Text = this.openTerrariaExeDialog.FileName;
                }
                catch (OptionsEntry.InvalidOptionsPathException ex)
                {
                    MessageBox.Show(ex.Message, "エラー", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                catch (FileNotFoundException ex2)
                {
                    MessageBox.Show(ex2.Message, "エラー", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            OptionsEntry instance = OptionsEntry.GetInstance();
            try
            {
                if (this.txtTerrariaExePath.Text == this.TerrarisExePath)
                {
                    this.Changed = false;
                }
                else
                {
                    instance.CheckTerrariaExe(this.txtTerrariaExePath.Text);
                    this.TerrarisExePath = this.txtTerrariaExePath.Text;
                    this.Changed = true;
                }

                this.canClose = true;
            }
            catch (OptionsEntry.InvalidOptionsPathException ex)
            {
                MessageBox.Show(ex.Message, "エラー", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                this.canClose = false;
            }
            catch (FileNotFoundException ex2)
            {
                MessageBox.Show(ex2.Message, "エラー", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                this.canClose = false;
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.TerrarisExePath = string.Empty;
            this.Changed = false;
            this.Close();
        }

        private void FormExePath_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.canClose)
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }

            this.canClose = true;
        }
    }
}

@rem **************************************
@rem tModLoader の日本語化バッチファイル
@rem 固有名詞日本語版
@rem **************************************
@rem -i オリジナルの tModLoader.exe のパス。
@rem -o 日本語化された tModLoader.exe のパス。
@rem -s 翻訳シートのパス。
@rem -t 非翻訳テーブルのパス。省略時は非翻訳テーブルを使用しない。
@rem -r 日本語化された tModLoader.exe が既に存在する場合はを上書きする。
@SET PATH="tools";%PATH%

tModLoaderJpAid.exe ^
	-i data\en\tModLoader.exe ^
	-o data\jp\tModLoader.exe ^
	-s data\csv\TerrariaTransSheet_v1.3.5.001.csv ^
	-r

@if not "%ERRORLEVEL%"  == "0" GOTO ERROR

@goto NORMAL

@rem *******************************************************************************
:ERROR
@echo;
@echo ********************************
@echo 日本語化MODの作成に失敗しました。
@echo ********************************
@echo;
@goto FINAL

@rem *******************************************************************************
:NORMAL
@echo;
@echo 日本語化MOD（固有名詞日本語版）の作成に成功しました。
@echo;
@goto FINAL

@rem *******************************************************************************
:FINAL
@pause
@exit /b

(2020.06.19)

tModLoader 日本語化MOD作成支援ツール（TMJA）v1.0.0.0

このツールは、ESteam版 tModLoader の日本語化を支援するツールです。

使用方法の詳細は、ブログの記事を御覧ください。
「synctam: tModLoader 日本語化支援ツールの使い方」
https://synctam.blogspot.com/2020/06/tmodloader.html


変更履歴
2020.06.19 新規公開

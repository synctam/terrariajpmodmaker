namespace TModLoaderJpAid
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MonoOptions;
    using S5mDebugTools;

    /// <summary>
    /// 旧 Terraria 用翻訳シートを読み込み、日本語化された Terraria.exe を作成する。
    /// -i: オリジナルの Terraria.exe のパス
    /// -o: 日本語化された Terraria.exe のパス
    /// -s: 翻訳シートのパス
    /// -n: アイテム関連を日本語化しない
    /// </summary>
    public class Program
    {
        private static int Main(string[] args)
        {
            var result = 0;

            var opt = new TOptions(args);
            if (opt.IsError)
            {
                result = 1;
            }
            else if (opt.Arges.Help)
            {
                opt.ShowUsage();

                result = 1;
            }
            else
            {
                try
                {
                    TmlJpMod.MakeMod(
                        opt.Arges.FileNameInput,
                        opt.Arges.FileNameOutput,
                        opt.Arges.FileNameTransSheet,
                        opt.Arges.FileNameUntranslationFile);

                    result = 0;
                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                    Console.WriteLine(msg);

                    result = 1;
                }
            }

            TDebugUtils.Pause();
            return result;
        }
    }
}

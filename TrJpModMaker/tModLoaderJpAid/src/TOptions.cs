// ******************************************************************************
// Copyright (c) 2015-2019 synctam
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

namespace MonoOptions
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Mono.Options;

    /// <summary>
    /// コマンドライン オプション
    /// </summary>
    public class TOptions
    {
        //// ******************************************************************************
        //// Property fields
        //// ******************************************************************************
        private TArgs args;
        private bool isError = false;
        private StringWriter errorMessage = new StringWriter();
        private OptionSet optionSet;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="arges">コマンドライン引数</param>
        public TOptions(string[] arges)
        {
            this.args = new TArgs();
            this.Settings(arges);
            if (this.IsError)
            {
                this.ShowErrorMessage();
                this.ShowUsage();
            }
            else
            {
                this.CheckOption();
                if (this.IsError)
                {
                    this.ShowErrorMessage();
                    this.ShowUsage();
                }
                else
                {
                    // skip
                }
            }
        }

        //// ******************************************************************************
        //// Property
        //// ******************************************************************************

        /// <summary>
        /// コマンドライン オプション
        /// </summary>
        public TArgs Arges { get { return this.args; } }

        /// <summary>
        /// コマンドライン オプションのエラー有無
        /// </summary>
        public bool IsError { get { return this.isError; } }

        /// <summary>
        /// エラーメッセージ
        /// </summary>
        public string ErrorMessage { get { return this.errorMessage.ToString(); } }

        /// <summary>
        /// Uasgeを表示する
        /// </summary>
        public void ShowUsage()
        {
            TextWriter writer = Console.Error;
            this.ShowUsage(writer);
        }

        /// <summary>
        /// Uasgeを表示する
        /// </summary>
        /// <param name="textWriter">出力先</param>
        public void ShowUsage(TextWriter textWriter)
        {
            StringWriter msg = new StringWriter();

            string exeName = Path.GetFileName(Environment.GetCommandLineArgs()[0]);
            msg.WriteLine(string.Empty);
            msg.WriteLine($@"使い方：");
            msg.WriteLine($@"日本語化MODを作成する。");
            msg.WriteLine(
                $@"  usage: {exeName} -i <original tModLoader path> -o <japanized tModLoader path>" +
                $@" -s <Trans Sheet path> [-n] [-r]");
            msg.WriteLine($@"OPTIONS:");
            this.optionSet.WriteOptionDescriptions(msg);
            msg.WriteLine($@"Example:");
            msg.WriteLine($@"  翻訳シート(-s)とオリジナルの tModLoader.exe(-i)から日本語化MOD(-o)を作成する。");
            msg.WriteLine(
                $@"    {exeName} -i data\en\tModLoader.exe -o data\jp\tModLoader.exe" +
                $@" -s data\csv\TransSheet.csv");
            msg.WriteLine($@"終了コード:");
            msg.WriteLine($@" 0  正常終了");
            msg.WriteLine($@" 1  異常終了");
            msg.WriteLine();

            if (textWriter == null)
            {
                textWriter = Console.Error;
            }

            textWriter.Write(msg.ToString());
        }

        /// <summary>
        /// エラーメッセージ表示
        /// </summary>
        public void ShowErrorMessage()
        {
            TextWriter writer = Console.Error;
            this.ShowErrorMessage(writer);
        }

        /// <summary>
        /// エラーメッセージ表示
        /// </summary>
        /// <param name="textWriter">出力先</param>
        public void ShowErrorMessage(TextWriter textWriter)
        {
            if (textWriter == null)
            {
                textWriter = Console.Error;
            }

            textWriter.Write(this.ErrorMessage);
        }

        /// <summary>
        /// オプション文字の設定
        /// </summary>
        /// <param name="args">args</param>
        private void Settings(string[] args)
        {
            this.optionSet = new OptionSet()
            {
                { "i|input="   , this.args.FileNameInputText       , v => this.args.FileNameInput        = v},
                { "o|output="  , this.args.FileNameOutputText      , v => this.args.FileNameOutput       = v},
                { "s|sheet="   , this.args.FileNameTransSheetText  , v => this.args.FileNameTransSheet   = v},
                { "t|="        , this.args.FileNameUntranslationFileText, v => this.args.FileNameUntranslationFile = v},
                { "r"          , this.args.UseReplaceText          , v => this.args.UseReplace           = v != null},
                { "h|help"     , "ヘルプ"                          , v => this.args.Help                 = v != null},
            };

            List<string> extra;
            try
            {
                extra = this.optionSet.Parse(args);
                if (extra.Count > 0)
                {
                    // 指定されたオプション以外のオプションが指定されていた場合、
                    // extra に格納される。
                    // 不明なオプションが指定された。
                    this.SetErrorMessage($"{Environment.NewLine}エラー：不明なオプションが指定されました。");
                    extra.ForEach(t => this.SetErrorMessage(t));
                    this.isError = true;
                }
            }
            catch (OptionException e)
            {
                ////パースに失敗した場合OptionExceptionを発生させる
                this.SetErrorMessage(e.Message);
                this.isError = true;
            }
        }

        /// <summary>
        /// オプションのチェック
        /// </summary>
        private void CheckOption()
        {
            //// -h
            if (this.Arges.Help)
            {
                this.SetErrorMessage();
                this.isError = false;
                return;
            }

            if (this.IsErrorInputFile())
            {
                return;
            }

            if (this.IsErrorOutputFile())
            {
                return;
            }

            if (this.IsErrorTransSheetFile())
            {
                return;
            }

            if (this.IsErrorUnTranslationFile())
            {
                return;
            }

            this.isError = false;
            return;
        }

        /// <summary>
        /// オリジナルの tModLoader.exe のエラー有無を返す
        /// </summary>
        /// <returns>オリジナルの tModLoader.exe のエラー有無</returns>
        private bool IsErrorInputFile()
        {
            if (string.IsNullOrWhiteSpace(this.Arges.FileNameInput))
            {
                this.SetErrorMessage(
                    $@"{Environment.NewLine}エラー：(-i)オリジナルの tModLoader.exe のパスを指定してください。");
                this.isError = true;

                return true;
            }
            else
            {
                if (!File.Exists(this.Arges.FileNameInput))
                {
                    this.SetErrorMessage(
                        $"{Environment.NewLine}エラー：(-i)オリジナルの tModLoader.exe が見つかりません。" +
                        $"{Environment.NewLine}({Path.GetFullPath(this.Arges.FileNameInput)})");
                    this.isError = true;

                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 日本語化された tModLoader.exe のエラー有無を返す
        /// </summary>
        /// <returns>日本語化された tModLoader.exe のエラー有無</returns>
        private bool IsErrorOutputFile()
        {
            if (string.IsNullOrWhiteSpace(this.Arges.FileNameOutput))
            {
                this.SetErrorMessage(
                    $@"{Environment.NewLine}エラー：(-o)出力ファイルのパスを指定してください。");
                this.isError = true;

                return true;
            }

            if (File.Exists(this.Arges.FileNameOutput) && !this.Arges.UseReplace)
            {
                this.SetErrorMessage($@"エラー：(-o)出力ファイルがすでに存在します。{Environment.NewLine}上書きする場合は -r オプションを指定してください。");
                this.isError = true;

                return true;
            }

            return false;
        }

        /// <summary>
        /// 翻訳シートのエラー有無を過返す
        /// </summary>
        /// <returns>翻訳シートのエラー有無</returns>
        private bool IsErrorTransSheetFile()
        {
            if (string.IsNullOrWhiteSpace(this.Arges.FileNameTransSheet))
            {
                this.SetErrorMessage(
                    $@"{Environment.NewLine}エラー：(-s)翻訳シートのパスを指定してください。");
                this.isError = true;

                return true;
            }
            else
            {
                if (!File.Exists(this.Arges.FileNameTransSheet))
                {
                    this.SetErrorMessage(
                        $"{Environment.NewLine}エラー：(-s)翻訳シートが見つかりません。" +
                        $"{Environment.NewLine}({Path.GetFullPath(this.Arges.FileNameTransSheet)})");
                    this.isError = true;

                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 非翻訳テーブルファイルのエラーの有無を返す
        /// </summary>
        /// <returns>非翻訳テーブルファイルのエラーの有無</returns>
        private bool IsErrorUnTranslationFile()
        {
            if (string.IsNullOrWhiteSpace(this.Arges.FileNameUntranslationFile))
            {
                //// 非翻訳テーブルなしで処理する。
                return false;
            }
            else
            {
                if (!File.Exists(this.Arges.FileNameUntranslationFile))
                {
                    this.SetErrorMessage(
                        $"{Environment.NewLine}エラー：(-t)非翻訳テーブルファイルが見つかりません。" +
                        $"{Environment.NewLine}({Path.GetFullPath(this.Arges.FileNameUntranslationFile)})");
                    this.isError = true;

                    return true;
                }
            }

            return false;
        }

        private void SetErrorMessage(string errorMessage = null)
        {
            if (errorMessage != null)
            {
                this.errorMessage.WriteLine(errorMessage);
            }
        }

        /// <summary>
        /// オプション項目
        /// </summary>
        public class TArgs
        {
            public string FileNameInput { get; internal set; }

            public string FileNameInputText { get; internal set; } =
                $"オリジナルの tModLoader.exe のパス。";

            public string FileNameOutput { get; internal set; }

            public string FileNameOutputText { get; internal set; } =
                $"日本語化された tModLoader.exe のパス。";

            public string FileNameTransSheet { get; internal set; }

            public string FileNameTransSheetText { get; internal set; } =
                $"翻訳シートのパス。";

            public string FileNameUntranslationFile { get; internal set; }

            public string FileNameUntranslationFileText { get; internal set; } =
                $"非翻訳（日本語化しない項目）テーブルのパス。";

            public bool UseReplace { get; internal set; }

            public string UseReplaceText { get; internal set; } =
                $"日本語化された tModLoader.exe が既に存在する場合はを上書きする。";

            public bool Help { get; set; }
        }
    }
}

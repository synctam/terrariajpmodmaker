namespace TModLoaderJpAid
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using TerrariaJpModLib.Models.LocalizeFile;
    using TerrariaJpModLib.Models.Mod;
    using TerrariaJpModLib.Models.TransSheet;
    using TerrariaJpModLib.UntranslationCommand;
    using TerrariaJpModLib.UnTranslationTable;

    public class TmlJpMod
    {
        public static void MakeMod(
            string exePath,
            string exePathJp,
            string sheetPath,
            string untranslationFilePath)
        {
            var sheet = TrTransSheetDao.LoadFromCsv(sheetPath);
            var res = TrContentDao.LoadFromResource(exePath, true);

            //// 非翻訳テーブル
            var untranslatedTableInfo = new TrUntranslationTableInfo();
            if (untranslationFilePath == null)
            {
                //// 指定がない場合は、非翻訳テーブルを使用しない。
            }
            else
            {
                TrUntranslationTableDao.LoadFromCsv(untranslatedTableInfo, untranslationFilePath);
            }

            //// 非翻訳コマンドは使用しない。
            var untranslatedCommandInfo = new TrUntranslationCommandInfo();

            TrModMaker.ModMake(
                res,
                sheet,
                exePath,
                exePathJp,
                untranslatedTableInfo,
                untranslatedCommandInfo);
        }
    }
}

namespace TrJpSheetMaker
{
    using System;
    using System.Collections.Generic;
    using S5mDebugTools;
    using TerarriaCompareLib.Language;
    using TerarriaCompareLib.Model.ParaTranz.Translated;
    using TerarriaCompareLib.Model.TransSheet;
    using TerrariaJpModLib.UnRegistrationTable;
    using TerrariaJpModLib.UnTranslationTable;
    using TrResouceEditLib.ResourceIO;

    public class Program
    {
        private const string ExePath = @"data\exe\Terraria.exe";

        private static int Main(string[] args)
        {
            if (args.Length == 1)
            {
                switch (args[0].ToLower())
                {
                    case "--untranslationtable":
                        MakeUntranslationTable();
                        break;
                    case "--updatemaster":
                        MakeParaTranzCsv();
                        break;
                    case "--makesheet":
                        //// TMM用翻訳シート作成
                        MakeTmmSheet();
                        //// Steam Workshop用jsonファイル作成。
                        break;
                    default:
                        var msg = @"--updatemaster or --makesheet";
                        Console.WriteLine($"Error: Unknown params({args[0]}). param must be {msg}.");
                        return 1;
                }
            }

            ////MakeCompareList();
            ////MakeUntranslationTable();
            ////MakeParaTranzCompareList();

            TDebugUtils.Pause();
            return 0;
        }

        private static void MakeParaTranzCompareList()
        {
            //// EXEのリソースから言語情報(英語)を作成。
            var trPath = ExePath;
            var langInfoEn = new TrLangInfo();
            TrSheetDao.GetLangDataFromResource(langInfoEn, trPath);

            //// ParaTranz の翻訳済みのrawデータから言語情報(日本語)を作成する。
            //// 翻訳済 & レビュー済み
            var stages = new TrParaTranzJsonDao.NStage[]
            {
                ////TrParaTranzJsonDao.NStage.UnTranslated,
                ////TrParaTranzJsonDao.NStage.Translated,
                ////TrParaTranzJsonDao.NStage.NeedsWork,
                TrParaTranzJsonDao.NStage.ProofRead,
                ////TrParaTranzJsonDao.NStage.Locked,
                ////TrParaTranzJsonDao.NStage.Hidden,
            };
            var langInfoJp = new TrLangInfo();
            TrParaTranzJsonDao.LoadFromParaTransData(langInfoJp, @"data\ParaTranz\raw\Terraria.Localization.Content.en-US.Game.json.csv.json",true, stages);
            TrParaTranzJsonDao.LoadFromParaTransData(langInfoJp, @"data\ParaTranz\raw\Terraria.Localization.Content.en-US.Items.json.csv.json", true, stages);
            TrParaTranzJsonDao.LoadFromParaTransData(langInfoJp, @"data\ParaTranz\raw\Terraria.Localization.Content.en-US.json.csv.json", true, stages);
            TrParaTranzJsonDao.LoadFromParaTransData(langInfoJp, @"data\ParaTranz\raw\Terraria.Localization.Content.en-US.Legacy.json.csv.json", true, stages);
            TrParaTranzJsonDao.LoadFromParaTransData(langInfoJp, @"data\ParaTranz\raw\Terraria.Localization.Content.en-US.NPCs.json.csv.json", true, stages);
            TrParaTranzJsonDao.LoadFromParaTransData(langInfoJp, @"data\ParaTranz\raw\Terraria.Localization.Content.en-US.Projectiles.json.csv.json", true, stages);
            TrParaTranzJsonDao.LoadFromParaTransData(langInfoJp, @"data\ParaTranz\raw\Terraria.Localization.Content.en-US.Town.json.csv.json", true, stages);

            TrJpCompare.CompareLang(langInfoEn, langInfoJp);
        }

        /// <summary>
        /// 非翻訳テーブル作成
        /// </summary>
        private static void MakeUntranslationTable()
        {
            //// EXEのリソースから言語情報(英語)を作成。
            var trPath = ExePath;
            var langInfoEn = new TrLangInfo();
            TrSheetDao.GetLangDataFromResource(langInfoEn, trPath);

            var transInfo = new TrUntranslationTableInfo();
            foreach (var langFile in langInfoEn.Items.Values)
            {
                foreach (var langGroup in langFile.Items.Values)
                {
                    var transEntry = new TrUntranslationTableEntry();
                    transEntry.FileID = langFile.FileID;
                    transEntry.Group = langGroup.Group;
                    transEntry.IsUntranslation = false;
                    transInfo.AddEntry(transEntry);
                }
            }

            TrUntranslationTableDao.SaveToCsv(transInfo, @"UntranslationTable(sample)_1.4.3.csv");
        }

        /// <summary>
        /// TMM用翻訳シートの作成。
        /// </summary>
        private static void MakeTmmSheet()
        {
            //// 翻訳済 & レビュー済み
            var stages = new TrParaTranzJsonDao.NStage[]
            {
                ////TrParaTranzJsonDao.NStage.UnTranslated,
                TrParaTranzJsonDao.NStage.Translated,
                TrParaTranzJsonDao.NStage.NeedsWork,
                TrParaTranzJsonDao.NStage.ProofRead,
                ////TrParaTranzJsonDao.NStage.Locked,
                ////TrParaTranzJsonDao.NStage.Hidden,
            };

            //// ParaTranz の翻訳済みのrawデータから言語情報(日本語)を作成する。
            var langInfoJp = new TrLangInfo();
            TrParaTranzJsonDao.LoadFromParaTransData(langInfoJp, @"data\ParaTranz\raw\Terraria.Localization.Content.en-US.Game.json.csv.json", false, stages);
            TrParaTranzJsonDao.LoadFromParaTransData(langInfoJp, @"data\ParaTranz\raw\Terraria.Localization.Content.en-US.Items.json.csv.json", false, stages);
            TrParaTranzJsonDao.LoadFromParaTransData(langInfoJp, @"data\ParaTranz\raw\Terraria.Localization.Content.en-US.json.csv.json", false, stages);
            TrParaTranzJsonDao.LoadFromParaTransData(langInfoJp, @"data\ParaTranz\raw\Terraria.Localization.Content.en-US.Legacy.json.csv.json", false, stages);
            TrParaTranzJsonDao.LoadFromParaTransData(langInfoJp, @"data\ParaTranz\raw\Terraria.Localization.Content.en-US.NPCs.json.csv.json", false, stages);
            TrParaTranzJsonDao.LoadFromParaTransData(langInfoJp, @"data\ParaTranz\raw\Terraria.Localization.Content.en-US.Projectiles.json.csv.json", false, stages);
            TrParaTranzJsonDao.LoadFromParaTransData(langInfoJp, @"data\ParaTranz\raw\Terraria.Localization.Content.en-US.Town.json.csv.json", false, stages);

            //// EXEのリソースから言語情報(英語)を作成。
            var trPath = ExePath;
            var langInfoEn = new TrLangInfo();
            TrSheetDao.GetLangDataFromResource(langInfoEn, trPath);

            //// 言語情報(英語)と言語情報(日本語)からTMM用翻訳シートを作成する。
            TrSheetDao.SaveToCsvForTmm(langInfoEn, langInfoJp, @"TerrariaTranslationSheet.csv");
        }

        /// <summary>
        /// オリジナルのEXEと既存の翻訳シートから以下を作成する。
        /// 1.オリジナルと翻訳シートの原文の変更一覧を出力する。
        /// </summary>
        private static void MakeCompareList()
        {
            //// EXEのリソースから言語情報を作成。
            var trPath = ExePath;
            var langInfoMasterEn = new TrLangInfo();
            TrSheetDao.GetLangDataFromResource(langInfoMasterEn, trPath);

            var langInfoTranEn = new TrLangInfo();
            var langInfoTranJp = new TrLangInfo();
            //// 旧翻訳シートから、英語版と日本語版の言語情報を作成。
            TrSheetDao.LoadFromCsv(langInfoTranEn, langInfoTranJp, @"data\csv\org\Terraria翻訳シート(有志翻訳) - vanilla.csv");

            //// 翻訳シートとリソースの言語情報を比較し結果リストを出力する。
            TrJpCompare.CompareLang(langInfoMasterEn, langInfoTranEn);
        }

        /// <summary>
        /// オリジナルのEXEと既存の翻訳シートから以下を作成する。
        /// 1.ParaTranz用翻訳シートを作成する。
        /// 2.ParaTranz用更新シートを作成する。
        /// 3.ParaTranz用翻訳更新用翻訳シートを作成する。
        /// 3.翻訳シートから用語集を作成する。
        /// </summary>
        private static void MakeParaTranzCsv()
        {
            var langInfoTranEn = new TrLangInfo();
            var langInfoTranJp = new TrLangInfo();
            //// 旧翻訳シートから、英語版と日本語版の言語情報を作成。
            TrSheetDao.LoadFromCsv(
                langInfoTranEn, langInfoTranJp, @"data\csv\org\Terraria翻訳シート(有志翻訳) - vanilla.csv");

            //// data\csv\ParaTranz\updateMaster\UnRegistTable\UnRegistrationTable.csv
            var unRegistInfo = new TrUnRegistrationTableInfo();
            TrUnRegistrationTableDao.LoadFromCsv(
                unRegistInfo, @"data\csv\ParaTranz\updateMaster\UnRegistTable\UnRegistrationTable.csv");

            //// EXEのリソースから言語情報を作成。
            var trPath = ExePath;
            var langInfoMasterEn = new TrLangInfo();
            TrSheetDao.GetLangDataFromResource(langInfoMasterEn, trPath);

            //// ParaTranz 用CSVファイル作成。
            TrSheetDao.SaveToCsv4ParaTranzFromTransSheet(
                langInfoTranEn, langInfoTranJp, null, @"data\csv\ParaTranz\translated", false);
            //// 原文更新情報の作成。
            TrSheetDao.SaveToCsv4ParaTranzFromTransSheet(
                langInfoMasterEn, null, unRegistInfo, @"data\csv\ParaTranz\updateMaster", true);

            //// 用語集の作成。
            TrSheetDao.SaveToCsv4ParaTranzGlossary(
                langInfoTranEn, langInfoTranJp, @"data\json\ParaTranz\Glossary\TrJpGlossary.json");
        }

        private static void CompareVanillaAndTmod(string path, string csvPath, HashSet<string> files)
        {
            var trPath1 = ExePath;
            var jsonNamesVanilla = new HashSet<string>()
            {
                "Terraria.Localization.Content.en-US.Game.json",
                "Terraria.Localization.Content.en-US.Items.json",
                "Terraria.Localization.Content.en-US.json",
                "Terraria.Localization.Content.en-US.Legacy.json",
                "Terraria.Localization.Content.en-US.NPCs.json",
                "Terraria.Localization.Content.en-US.Projectiles.json",
                "Terraria.Localization.Content.en-US.Town.json"
            };

            var trPath2 = @"C:\Program Files (x86)\Steam\steamapps\common\tModLoader\tModLoader.exe";
            var jsonNamesTMod = new HashSet<string>()
            {
                "Terraria.Localization.Content.en-US.Game.json",
                "Terraria.Localization.Content.en-US.Items.json",
                "Terraria.Localization.Content.en-US.Legacy.json",
                "Terraria.Localization.Content.en-US.Main.json",
                "Terraria.Localization.Content.en-US.NPCs.json",
                "Terraria.Localization.Content.en-US.Projectiles.json",
                "Terraria.Localization.Content.en-US.tModLoader.json",
                "Terraria.Localization.Content.en-US.Town.json"
            };

            var infoVanilla = CompareLang(trPath1, @"TrTransSheet_Vanilla.csv", jsonNamesVanilla);
            var infoTMod = CompareLang(trPath2, @"TrTransSheet_tModLoader.csv", jsonNamesTMod);
            TrJpCompare.CompareLang(infoVanilla, infoTMod);
        }

        private static TrLangInfo CompareLang(string path, string csvPath, HashSet<string> files)
        {
            var sheet = new TrJpSheetMaker();
            var langInfoTranEn = new TrLangInfo();
            foreach (var resourceName in files)
            {
                using (var ms = TrResourceUtils.GetResource(path, resourceName))
                {
                    if (ms == null)
                    {
                        Console.WriteLine($"{resourceName} is null.");
                    }
                    else
                    {
                        sheet.MakeLangInfo(langInfoTranEn, ms, resourceName);
                        TrSheetDao.SaveToCsv(langInfoTranEn, csvPath);
                    }
                }
            }

            return langInfoTranEn;
        }
    }
}

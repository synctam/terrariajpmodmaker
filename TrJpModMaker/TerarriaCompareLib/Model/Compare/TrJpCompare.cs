namespace TrJpSheetMaker
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using System.Threading.Tasks;
    using TerarriaCompareLib.Language;

    public class TrJpCompare
    {
        public static void CompareLang(TrLangInfo master, TrLangInfo tran)
        {
            using (StreamWriter swMaster = new StreamWriter(@"TransSheet_Diff_Master.txt", false, Encoding.UTF8))
            using (StreamWriter swTran = new StreamWriter(@"TransSheet_Diff_Tran.txt", false, Encoding.UTF8))
            using (StreamWriter swSame = new StreamWriter(@"TransSheet_same.txt", false, Encoding.UTF8))
            using (StreamWriter swNew = new StreamWriter(@"TransSheet_new.txt", false, Encoding.UTF8))
            {
                foreach (var file in master.Items.Values)
                {
                    foreach (var group in file.Items.Values)
                    {
                        foreach (var entry in group.Items.Values)
                        {
                            if (IsInvalidNumber(entry.Text))
                            {
                                Console.WriteLine($"Invalid number. FileID({file.FileID}) Group({group.Group}) Key({entry.Key}) Text({entry.Text})");
                            }

                            var tranEntry = tran.GetEntry(file.FileID, group.Group, entry.Key);
                            if (tranEntry == null)
                            {
                                //// 新規
                                swNew.WriteLine($"File({file.FileID}) Group({group.Group}) key({entry.Key})");
                            }
                            else
                            {
                                if (entry.Text.Equals(tranEntry.Text, StringComparison.OrdinalIgnoreCase))
                                {
                                    //// 同一
                                    swSame.WriteLine($"File({file.FileID}) Group({group.Group}) key({entry.Key})");
                                }
                                else
                                {
                                    //// 更新があったエントリー
                                    swMaster.WriteLine($"File({file.FileID}) Group({group.Group}) key({entry.Key}) Value({Ctrl2Tag(entry.Text)})");
                                    swTran.WriteLine($"File({file.FileID}) Group({group.Group}) key({tranEntry.Key}) Value({Ctrl2Tag(tranEntry.Text)})");
                                }
                            }
                        }
                    }
                }
            }

            using (StreamWriter swDel = new StreamWriter(@"TransSheet_del.txt", false, Encoding.UTF8))
            {
                foreach (var file in tran.Items.Values)
                {
                    foreach (var group in file.Items.Values)
                    {
                        foreach (var entry in group.Items.Values)
                        {
                            var masterEntry = master.GetEntry(file.FileID, group.Group, entry.Key);
                            if (masterEntry == null)
                            {
                                //// 削除
                                swDel.WriteLine($"File({file.FileID}) Group({group.Group}) key({entry.Key})");
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// ExcelなどでCSVファイルからインポートする時に、数値として認識される可能性の有無を返す。
        /// 数値として認識されると、行頭の符号が削除されるケースがあるため。
        /// </summary>
        /// <param name="text">テキスト</param>
        /// <returns>数値として認識される可能性の有無</returns>
        private static bool IsInvalidNumber(string text)
        {
            if (text.StartsWith("-") || text.StartsWith("+"))
            {
                int num = 0;
                if (int.TryParse(text, out num))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (text.StartsWith("."))
            {
                double num = 0.0;

                if (double.TryParse(text, out num))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (text.StartsWith("@"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static string Ctrl2Tag(string text)
        {
            StringBuilder buff = new StringBuilder(text);

            buff.Replace("\r\n", "<CRLF>");
            buff.Replace("\r", "<CR>");
            ////buff.Replace("\n", "<LF>");
            buff.Replace("\n", "\\n");
            buff.Replace("\t", "<TAB>");

            return buff.ToString();
        }
    }
}

namespace TerarriaCompareLib.Language
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class TrLangFile
    {
        public TrLangFile(string fileID)
        {
            this.FileID = fileID;
        }

        /// <summary>
        /// 言語グループの辞書。
        /// キーはグループ名。
        /// </summary>
        public Dictionary<string, TrLangGroup> Items { get; } =
            new Dictionary<string, TrLangGroup>(StringComparer.OrdinalIgnoreCase);

        public string FileID { get; } = string.Empty;

        public void AddGroup(TrLangGroup group)
        {
            if (this.Items.ContainsKey(group.Group))
            {
                var currentGroup = this.Items[group.Group];
                foreach (var entry in group.Items.Values)
                {
                    currentGroup.AddEntry(entry);
                }
            }
            else
            {
                this.Items.Add(group.Group, group);
            }
        }

        public TrLangEntry GetEntry(string group, string key)
        {
            if (this.Items.ContainsKey(group))
            {
                var groupObj = this.Items[group];
                return groupObj.GetEntry(key);
            }
            else
            {
                return null;
            }
        }

        public string ToString(string tab)
        {
            var buff = new StringBuilder();

            buff.AppendLine($"{tab}({this.FileID})");
            tab = tab + "\t";
            foreach (var langGroup in this.Items.Values)
            {
                buff.Append($"{langGroup.ToString(tab)}");
            }

            return buff.ToString();
        }
    }
}

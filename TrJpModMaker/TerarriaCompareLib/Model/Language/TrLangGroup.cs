namespace TerarriaCompareLib.Language
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class TrLangGroup
    {
        public TrLangGroup(string group, TrLangFile parent)
        {
            this.Group = group;
            this.Parent = parent;
        }

        /// <summary>
        /// 言語エントリーの辞書。
        /// キーはキー。
        /// </summary>
        public Dictionary<string, TrLangEntry> Items { get; } =
            new Dictionary<string, TrLangEntry>(StringComparer.OrdinalIgnoreCase);

        public TrLangFile Parent { get; } = null;

        public string Group { get; set; }

        public void AddEntry(TrLangEntry entry)
        {
            if (TrLangInfo.IsVariableOnry(entry.Text))
            {
                //// テキストが変数のみの場合は登録しない。
            }
            else
            {
                if (this.Items.ContainsKey(entry.Key))
                {
                    //// 既に存在している時は更新する。
                    //// Terraria では、後から登録したものが優先的に使用される（Terraria: LanguageManager.cs 参照）
                    var currentEntry = this.Items[entry.Key];
                    currentEntry.Text = entry.Text;
                    Console.WriteLine($"Replaced entry. FileID({entry.Parent.Parent.FileID}) Group({entry.Parent.Group}) ({entry.Key})");
                }
                else
                {
                    //// 新規登録
                    this.Items.Add(entry.Key, entry);
                }
            }
        }

        public TrLangEntry GetEntry(string key)
        {
            if (this.Items.ContainsKey(key))
            {
                return this.Items[key];
            }
            else
            {
                return null;
            }
        }

        public string ToString(string tab)
        {
            var buff = new StringBuilder();

            buff.AppendLine($"{tab}({this.Group})");
            tab = tab + "\t";
            foreach (var entry in this.Items.Values)
            {
                buff.Append($"{entry.ToString(tab)}");
            }

            return buff.ToString();
        }
    }
}

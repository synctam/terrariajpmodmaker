namespace TerarriaCompareLib.Language
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using TerrariaJpModLib.Models.TransSheet;

    public class TrLangEntry
    {
        public TrLangEntry(string key, string text, TrLangGroup parent)
        {
            this.Key = key;
            if (TrTransSheetDao.IsInvalidCtrlCodeString(text))
            {
                this.IsInvalidText = true;
            }

            this.Text = text;
            this.Parent = parent;
        }

        public TrLangGroup Parent { get; } = null;

        public string Key { get; set; }

        public string Text { get; set; }

        /// <summary>
        /// テキストに誤り（改行文字）が含まれるかどうか。
        /// </summary>
        public bool IsInvalidText { get; } = false;

        public string ToString(string tab)
        {
            var buff = new StringBuilder();

            buff.AppendLine($"{tab}Key({this.Key}) Text({this.Text})");

            return buff.ToString();
        }
    }
}

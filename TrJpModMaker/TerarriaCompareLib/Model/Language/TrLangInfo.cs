namespace TerarriaCompareLib.Language
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;

    public class TrLangInfo
    {
        /// <summary>
        /// 言語ファイルの辞書。
        /// キーはファイルＩＤ。
        /// </summary>
        public Dictionary<string, TrLangFile> Items { get; } =
            new Dictionary<string, TrLangFile>(StringComparer.OrdinalIgnoreCase);

        /// <summary>
        /// テキストが変数のみかを判定する。
        /// </summary>
        /// <param name="text">テキスト</param>
        /// <returns>変数のみの場合は true</returns>
        public static bool IsVariableOnry(string text)
        {
            var reg = new Regex("{.*?}");
            var result = reg.Replace(text, string.Empty);
            if (string.IsNullOrWhiteSpace(result))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void AddFile(TrLangFile langFile)
        {
            if (this.Items.ContainsKey(langFile.FileID))
            {
                var currentlangFile = this.Items[langFile.FileID];
                foreach (var group in langFile.Items.Values)
                {
                    currentlangFile.AddGroup(group);
                }
            }
            else
            {
                this.Items.Add(langFile.FileID, langFile);
            }
        }

        public TrLangEntry GetEntry(string fileID, string group, string key)
        {
            if (this.Items.ContainsKey(fileID))
            {
                var file = this.Items[fileID];
                return file.GetEntry(group, key);
            }
            else
            {
                return null;
            }
        }

        public override string ToString()
        {
            var buff = new StringBuilder();

            var tab = string.Empty;
            foreach (var langFile in this.Items.Values)
            {
                buff.Append($"{tab}{langFile.ToString(tab)}");
            }

            return buff.ToString();
        }
    }
}

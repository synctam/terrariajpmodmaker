#pragma warning disable SA1008 // Opening parenthesis must be spaced correctly
namespace TerarriaCompareLib.Model.ParaTranz.Translated
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using TerarriaCompareLib.Language;
    using TerrariaJpModLib.Utils;
    using TrJpSheetMaker;

    public class TrParaTranzJsonDao
    {
        public enum NStage : long
        {
            /// <summary>
            /// 未翻訳
            /// </summary>
            UnTranslated = 0,

            /// <summary>
            /// 翻訳済み
            /// </summary>
            Translated = 1 << 0,

            /// <summary>
            /// 疑問あり
            /// </summary>
            NeedsWork = 1 << 1,

            /// <summary>
            /// レビュー済み
            /// </summary>
            ProofRead = 1 << 2,

            /// <summary>
            /// ロック中
            /// </summary>
            Locked = 1 << 3,

            /// <summary>
            /// 非表示(JSON上は -1 )
            /// </summary>
            Hidden = 1 << 4,
        }

        /// <summary>
        /// ParaTranz raw data を読み込み言語情報を作成する。
        /// 1.指定したstageの情報のみ作成対象とする。
        /// 2.原文と翻訳文が同一のものは除外する。
        /// </summary>
        /// <param name="langInfo">言語情報</param>
        /// <param name="path">rawファイルのパス</param>
        /// <param name="useOriginal">原文の使用有無</param>
        /// <param name="stages">読み込むstage</param>
        public static void LoadFromParaTransData(TrLangInfo langInfo, string path, bool useOriginal = false, NStage[] stages = default)
        {
            var jsonString = File.ReadAllText(path, Encoding.UTF8);
            var paraTranzJson = TrParaTranzDataWidthStatusSchema.FromJson(jsonString);
            int count = 0;
            int regist = 0;
            foreach (var entry in paraTranzJson)
            {
                var fileid = TrFileUtils.GetFileID(entry.Key);
                var group = TrFileUtils.GetGroup(entry.Key);
                var key = TrFileUtils.GetKey(entry.Key);

                var langFile = new TrLangFile(fileid);

                var langGroup = new TrLangGroup(group, langFile);
                langFile.AddGroup(langGroup);

                var langEntry = new TrLangEntry(key, entry.Translation, langGroup);
                if (useOriginal)
                {
                    langEntry = new TrLangEntry(key, entry.Original, langGroup);
                }

                if (langEntry.IsInvalidText)
                {
                    Console.WriteLine($"Invalid ctrl char. Key({langEntry.Key}) Text({langEntry.Text})");
                }

                var rc = Any(entry.Stage, stages);
                if (rc)
                {
                    if (!useOriginal && entry.Original.Equals(langEntry.Text))
                    {
                        //// 原文と翻訳文が同一のものは除外する。
                    }
                    else
                    {
                        langGroup.AddEntry(langEntry);
                        regist++;
                    }
                }
                else
                {
                    //// 異なるstageは除外
                }

                langInfo.AddFile(langFile);
                count++;
            }

            Console.WriteLine($"{path}={regist}/{count}");
        }

        private static bool Any(long stageNo, params NStage[] stages)
        {
            var stage = GetStageFlag(stageNo);
            foreach (var entry in stages)
            {
                if (stage.HasFlag(entry))
                {
                    return true;
                }
            }

            return false;
        }

        private static NStage GetStageFlag(long stageno)
        {
            switch (stageno)
            {
                case 0:
                    return NStage.UnTranslated;
                case 0b0001: // 1
                    return NStage.Translated;
                case 0b0010: // 2
                    return NStage.NeedsWork;
                case 0b0011: // 3
                    return NStage.Translated | NStage.NeedsWork;
                case 0b0100: // 4
                    return NStage.ProofRead;
                case 0b0101: // 5
                    return NStage.Translated | NStage.ProofRead;
                case 0b0110: // 6
                    return NStage.Translated | NStage.NeedsWork;
                case 0b0111: // 7
                    return NStage.Translated | NStage.NeedsWork | NStage.ProofRead;
                case 0b1000: // 8
                    return NStage.Locked;
                case -1:
                    return NStage.Hidden;
                default:
                    throw new Exception("Logic error.");
            }
        }
    }
}
#pragma warning restore SA1008 // Opening parenthesis must be spaced correctly

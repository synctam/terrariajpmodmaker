// <auto-generated />
namespace TerarriaCompareLib.Model.ParaTranz.Glossary
{
    //
    // To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
    //
    //    using ParaTransData;
    //
    //    var trParaTranzGlossarySchema = TrParaTranzGlossarySchema.FromJson(jsonString);

    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class TrParaTranzGlossarySchema
    {
        [JsonProperty("term")]
        public string Term { get; set; }

        [JsonProperty("translation")]
        public string Translation { get; set; }

        [JsonProperty("pos")]
        public string Pos { get; set; }

        [JsonProperty("note")]
        public string Note { get; set; }
    }

    public partial class TrParaTranzGlossarySchema
    {
        public static List<TrParaTranzGlossarySchema> FromJson(string json) =>
            JsonConvert.DeserializeObject<List<TrParaTranzGlossarySchema>>(json, Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this List<TrParaTranzGlossarySchema> self) =>
            JsonConvert.SerializeObject(self, Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}

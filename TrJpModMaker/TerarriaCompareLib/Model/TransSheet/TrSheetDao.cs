namespace TerarriaCompareLib.Model.TransSheet
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using CsvHelper;
    using TerarriaCompareLib.Language;
    using TerarriaCompareLib.Model.ParaTranz.Glossary;
    using TerrariaJpModLib.UnRegistrationTable;
    using TerrariaJpModLib.Utils;
    using TrResouceEditLib.ResourceIO;

    public class TrSheetDao
    {
        private static readonly HashSet<string> JsonNamesVanilla = new HashSet<string>()
            {
                "Terraria.Localization.Content.en-US.Game.json",
                "Terraria.Localization.Content.en-US.Items.json",
                "Terraria.Localization.Content.en-US.json",
                "Terraria.Localization.Content.en-US.Legacy.json",
                "Terraria.Localization.Content.en-US.NPCs.json",
                "Terraria.Localization.Content.en-US.Projectiles.json",
                "Terraria.Localization.Content.en-US.Town.json"
            };

        /// <summary>
        /// 翻訳シートから作成した言語情報から ParaTranz 用CSVファイルを作成する。
        /// </summary>
        /// <param name="langInfoTranEn">言語情報（英語）</param>
        /// <param name="langInfoTranJp">言語情報（日本語）</param>
        /// <param name="unRegistInfo">非登録情報</param>
        /// <param name="folderPath">ParaTranz 用CSVを出力するフォルダーのパス</param>
        /// <param name="isUpdate">原文更新用かどうか</param>
        public static void SaveToCsv4ParaTranzFromTransSheet(
            TrLangInfo langInfoTranEn,
            TrLangInfo langInfoTranJp,
            TrUnRegistrationTableInfo unRegistInfo,
            string folderPath,
            bool isUpdate)
        {
            foreach (var langFileEn in langInfoTranEn.Items.Values)
            {
                if (!IsAvailableFileID(langFileEn.FileID))
                {
                    //// 不要FileIDを除外する。
                    continue;
                }

                var path = Path.Combine(folderPath, langFileEn.FileID + ".csv");
                using (var writer = new CsvWriter(new StreamWriter(path, false, Encoding.UTF8)))
                {
                    writer.Configuration.RegisterClassMap<TrTransSheetCsvMapper4ParaTranz>();
                    //// ParaTrnaz ではヘッダー行は不要。

                    foreach (var langGroupEn in langFileEn.Items.Values)
                    {
                        if (!IsAvailableGroup(langFileEn.FileID, langGroupEn.Group))
                        {
                            //// 不要グループを除外する。
                            continue;
                        }

                        foreach (var langEntryEn in langGroupEn.Items.Values)
                        {
                            if (string.IsNullOrWhiteSpace(langEntryEn.Text))
                            {
                                //// テキストが空のものは除外する。
                                continue;
                            }

                            if (TrLangInfo.IsVariableOnry(langEntryEn.Text))
                            {
                                //// 変数のみのテキストは除外する。
                                continue;
                            }

                            if ((unRegistInfo != null) && unRegistInfo.IsUnRegistration(langFileEn.FileID, langGroupEn.Group))
                            {
                                //// ParaTranz に登録しない FileID:Group はスキップする。
                                continue;
                            }

                            var csvEntryEn = new TrTransSheet4ParaTranzCsv();
                            csvEntryEn.Key = TrFileUtils.ConvertParaTransKey(langFileEn.FileID, langGroupEn.Group, langEntryEn.Key);
                            csvEntryEn.Original = langEntryEn.Text;
                            if (isUpdate)
                            {
                                //// 原文更新用CSVファイル作成時
                                csvEntryEn.Translated = string.Empty;
                            }
                            else
                            {
                                //// 初期登録、または翻訳データインポート時
                                var langEntryJp = langInfoTranJp.GetEntry(langFileEn.FileID, langGroupEn.Group, langEntryEn.Key);
                                if (langEntryJp == null)
                                {
                                    csvEntryEn.Translated = string.Empty;
                                }
                                else
                                {
                                    csvEntryEn.Translated = langEntryJp.Text;
                                }
                            }

                            csvEntryEn.Context = $"Group({langGroupEn.Group})";

                            writer.WriteRecord(csvEntryEn);
                            writer.NextRecord();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 言語情報(英語)と言語情報(日本語)からTMM用CSVファイルを作成する。
        /// </summary>
        /// <param name="langInfoEn">言語情報(英語)</param>
        /// <param name="langInfoJp">言語情報(日本語)</param>
        /// <param name="path">TMM用CSVファイルのパス</param>
        public static void SaveToCsvForTmm(TrLangInfo langInfoEn, TrLangInfo langInfoJp, string path)
        {
            using (var writer = new CsvWriter(new StreamWriter(path, false, Encoding.UTF8)))
            {
                writer.Configuration.RegisterClassMap<TrTransSheetCsvCsvMapper>();
                writer.Configuration.HasHeaderRecord = true;
                writer.WriteHeader<TrTransSheetCsv>();
                writer.NextRecord();

                foreach (var langFileEN in langInfoEn.Items.Values)
                {
                    foreach (var langGroupEn in langFileEN.Items.Values)
                    {
                        foreach (var langEntryEn in langGroupEn.Items.Values)
                        {
                            var langEntryJp = langInfoJp.GetEntry(langFileEN.FileID, langGroupEn.Group, langEntryEn.Key);

                            //// 英文と翻訳文が同一のものも出力する（ユーザーが変更できるようにするため）
                            var csvEntry = new TrTransSheetCsv();
                            csvEntry.FileID = Path.GetFileNameWithoutExtension(langFileEN.FileID);
                            csvEntry.Group = langGroupEn.Group;
                            csvEntry.Key = langEntryEn.Key;
                            csvEntry.English = langEntryEn.Text;
                            if (langEntryJp == null)
                            {
                                csvEntry.Japanese = string.Empty;
                            }
                            else
                            {
                                csvEntry.Japanese = langEntryJp.Text;
                            }

                            writer.WriteRecord(csvEntry);
                            writer.NextRecord();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// ParaTranz用の用語集をJSON形式で出力する。
        /// </summary>
        /// <param name="langInfoTranEn">言語情報（英語）</param>
        /// <param name="langInfoTranJp">言語情報（日本語）</param>
        /// <param name="path">JSON形式の用語集のパス</param>
        public static void SaveToCsv4ParaTranzGlossary(
            TrLangInfo langInfoTranEn, TrLangInfo langInfoTranJp, string path)
        {
            List<TrParaTranzGlossarySchema> items = new List<TrParaTranzGlossarySchema>();
            foreach (var langFileEn in langInfoTranEn.Items.Values)
            {
                if (!langFileEn.FileID.Equals(@"Terraria.Localization.Content.en-US.Items.json", StringComparison.OrdinalIgnoreCase))
                {
                    continue;
                }

                foreach (var langGroupEn in langFileEn.Items.Values)
                {
                    if (!(langGroupEn.Group.Equals("PaintingArtist", StringComparison.OrdinalIgnoreCase) ||
                        langGroupEn.Group.Equals("ItemName", StringComparison.OrdinalIgnoreCase)))
                    {
                        continue;
                    }

                    foreach (var langEntryEn in langGroupEn.Items.Values)
                    {
                        var langEntryJp = langInfoTranJp.GetEntry(langFileEn.FileID, langGroupEn.Group, langEntryEn.Key);
                        if (langEntryJp == null)
                        {
                            continue;
                        }

                        if (langEntryEn.Text.Equals(langEntryJp.Text, StringComparison.OrdinalIgnoreCase))
                        {
                            continue;
                        }

                        if (string.IsNullOrWhiteSpace(langEntryJp.Text))
                        {
                            continue;
                        }

                        var glossaryEntry = new TrParaTranzGlossarySchema();
                        glossaryEntry.Pos = "noun";
                        glossaryEntry.Term = langEntryEn.Text;
                        glossaryEntry.Translation = langEntryJp.Text;

                        items.Add(glossaryEntry);
                    }
                }
            }

            var rc = Newtonsoft.Json.JsonConvert.SerializeObject(items);
            var utf8_encoding = new UTF8Encoding(false);
            File.WriteAllText(path, rc, utf8_encoding);
        }

        /// <summary>
        /// リソース内の言語情報を読み込み、言語情報オブジェクトにデータを格納する。
        /// ToDo: TerrariaModLibへの移行を検討する。
        /// </summary>
        /// <param name="langInfo">言語情報</param>
        /// <param name="path">EXEのパス</param>
        public static void GetLangDataFromResource(TrLangInfo langInfo, string path)
        {
            var trJpSheetMaker = new TrJpSheetMaker();

            foreach (var resourceName in JsonNamesVanilla)
            {
                using (var ms = TrResourceUtils.GetResource(path, resourceName))
                {
                    if (ms == null)
                    {
                        Console.WriteLine($"{resourceName} is null.");
                    }
                    else
                    {
                        trJpSheetMaker.MakeLangInfo(langInfo, ms, resourceName);
                    }
                }
            }
        }

        /// <summary>
        /// 既存の翻訳シートを読み込み、TrLangInfoオブジェクトに英語版、日本語版のデータを格納する。
        /// </summary>
        /// <param name="langInfoEn">英語版情報</param>
        /// <param name="langInfoJp">日本語版情報</param>
        /// <param name="path">翻訳シートのパス</param>
        /// <param name="enc">文字コード</param>
        public static void LoadFromCsv(TrLangInfo langInfoEn, TrLangInfo langInfoJp, string path, Encoding enc = null)
        {
            if (enc == null)
            {
                enc = Encoding.UTF8;
            }

            using (var reader = new StreamReader(path, enc))
            {
                using (var csv = new CsvReader(reader))
                {
                    csv.Configuration.Delimiter = ",";
                    csv.Configuration.HasHeaderRecord = true;
                    csv.Configuration.RegisterClassMap<TrTransSheetCsvCsvMapper>();

                    var records = csv.GetRecords<TrTransSheetCsv>();
                    foreach (var record in records)
                    {
                        var fileID = record.FileID + ".json";
                        var langFileEn = new TrLangFile(fileID);
                        var langFileJp = new TrLangFile(fileID);
                        {
                            var langGroupEn = new TrLangGroup(record.Group, langFileEn);
                            var langGroupJp = new TrLangGroup(record.Group, langFileJp);
                            {
                                var langEntryEn = new TrLangEntry(record.Key, record.English, langGroupEn);
                                var langEntryJp = new TrLangEntry(record.Key, record.Japanese, langGroupJp);
                                langGroupEn.AddEntry(langEntryEn);
                                langGroupJp.AddEntry(langEntryJp);
                            }

                            langFileEn.AddGroup(langGroupEn);
                            langFileJp.AddGroup(langGroupJp);
                        }

                        langInfoEn.AddFile(langFileEn);
                        langInfoJp.AddFile(langFileJp);
                    }
                }
            }
        }

        /// <summary>
        /// 言語情報からCSVファイルを作成する。
        /// </summary>
        /// <param name="langInfo">言語情報</param>
        /// <param name="path">CSVファイルのパス</param>
        public static void SaveToCsv(TrLangInfo langInfo, string path)
        {
            using (var writer = new CsvWriter(new StreamWriter(path, false, Encoding.UTF8)))
            {
                writer.Configuration.RegisterClassMap<CsvMapper>();
                writer.WriteHeader<TrLangEntryCsv>();
                writer.NextRecord();

                foreach (var langFile in langInfo.Items.Values)
                {
                    foreach (var langGroup in langFile.Items.Values)
                    {
                        int no = 0;
                        foreach (var langEntry in langGroup.Items.Values)
                        {
                            TrLangEntryCsv csvEntry = new TrLangEntryCsv();
                            csvEntry.FileID = langFile.FileID;
                            csvEntry.Group = langGroup.Group;
                            csvEntry.Key = langEntry.Key;
                            csvEntry.Text = langEntry.Text;
                            csvEntry.Sequence = no;
                            no++;
                            writer.WriteRecord(csvEntry);
                            writer.NextRecord();
                        }
                    }
                }
            }
        }

        private static string CompressFileID(string fileID)
        {
            return fileID.Replace("Terraria.Localization.Content.en-US", string.Empty);
        }

        private static string DecompressFileID(string fileID)
        {
            return "Terraria.Localization.Content.en-US." + fileID;
        }

        /// <summary>
        /// FileIDの使用可否を返す。
        /// </summary>
        /// <param name="fileID">FileID</param>
        /// <returns>FileIDの使用可否</returns>
        private static bool IsAvailableFileID(string fileID)
        {
            if (fileID.Equals("Terraria.Localization.Content.en-US.tModLoader.json", StringComparison.OrdinalIgnoreCase))
            {
                //// 翻訳シートから除外する
                return false;
            }

            return true;
        }

        /// <summary>
        /// グループの使用可否を返す。
        /// </summary>
        /// <param name="fileID">FileID</param>
        /// <param name="group">グループ名</param>
        /// <returns>グループの使用可否</returns>
        private static bool IsAvailableGroup(string fileID, string group)
        {
            if (fileID.Equals("Terraria.Localization.Content.en-US.json", StringComparison.OrdinalIgnoreCase) &&
                group.Equals("CLI", StringComparison.OrdinalIgnoreCase))
            {
                //// 翻訳シートから除外する
                return false;
            }

            return true;
        }

        /// <summary>
        /// レコード：CSV書き込み時はこのクラスの定義順に出力される。
        /// </summary>
        public class TrLangEntryCsv
        {
            public int Sequence { get; set; }

            public string FileID { get; set; }

            public string Group { get; set; }

            public string Key { get; set; }

            public string Text { get; set; }
        }

        /// <summary>
        /// 既存翻訳シート
        /// </summary>
        public class TrTransSheetCsv
        {
            public string FileID { get; set; }

            public string Group { get; set; }

            public string Key { get; set; }

            public string English { get; set; }

            public string Japanese { get; set; }
        }

        /// <summary>
        /// ParaTranz用翻訳シート
        /// </summary>
        public class TrTransSheet4ParaTranzCsv
        {
            /// <summary>
            /// キー
            /// キーは、fileID:group:key
            /// </summary>
            public string Key { get; set; }

            /// <summary>
            /// 原文
            /// </summary>
            public string Original { get; set; }

            /// <summary>
            /// 翻訳済みのテキスト
            /// </summary>
            public string Translated { get; set; }

            /// <summary>
            /// コメント（オプション）
            /// </summary>
            public string Context { get; set; }
        }

        /// <summary>
        /// 格納ルール ：
        /// </summary>
        public class CsvMapper : CsvHelper.Configuration.ClassMap<TrLangEntryCsv>
        {
            public CsvMapper()
            {
                // 出力時の列の順番は指定した順となる。
                this.Map(x => x.FileID).Name("[[FileID]]");
                this.Map(x => x.Group).Name("[[Group]]");
                this.Map(x => x.Key).Name("[[Key]]");
                this.Map(x => x.Text).Name("[[Text]]");
                this.Map(x => x.Sequence).Name("[[Sequence]]");
            }
        }

        /// <summary>
        /// 格納ルール：既存翻訳シート用CSVマッパー
        /// </summary>
        public class TrTransSheetCsvCsvMapper : CsvHelper.Configuration.ClassMap<TrTransSheetCsv>
        {
            public TrTransSheetCsvCsvMapper()
            {
                // 出力時の列の順番は指定した順となる。
                this.Map(x => x.FileID).Name("[[FileID]]");
                this.Map(x => x.Group).Name("[[Group]]");
                this.Map(x => x.Key).Name("[[Key]]");
                this.Map(x => x.English).Name("[[English]]");
                this.Map(x => x.Japanese).Name("[[Japanese]]");
            }
        }

        /// <summary>
        /// 格納ルール ：ParaTranz用
        /// </summary>
        public class TrTransSheetCsvMapper4ParaTranz : CsvHelper.Configuration.ClassMap<TrTransSheet4ParaTranzCsv>
        {
            public TrTransSheetCsvMapper4ParaTranz()
            {
                // 出力時の列の順番は指定した順となる。
                this.Map(x => x.Key).Name("[[Key]]");
                this.Map(x => x.Original).Name("[[Original]]");
                this.Map(x => x.Translated).Name("[[Translated]]");
                this.Map(x => x.Context).Name("[[Context]]");
            }
        }
    }
}

namespace TerarriaCompareLib.Model.TransSheet
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CsvHelper;
    using Newtonsoft.Json;
    using TerarriaCompareLib.Language;

    public class TrJpSheetMaker
    {
        public void MakeLangInfo(TrLangInfo langInfo, Stream st, string fileID)
        {
            TrLangGroup langGroup = null;
            var langFile = new TrLangFile(fileID);
            langInfo.AddFile(langFile);

            var key = string.Empty;
            var value = string.Empty;

            var reader = new StreamReader(st);
            var jtr = new JsonTextReader(reader);
            while (jtr.Read())
            {
                if (jtr.Depth == 1 && jtr.Value != null)
                {
                    var group = jtr.Value.ToString();
                    langGroup = new TrLangGroup(group, langFile);
                    langFile.AddGroup(langGroup);
                }
                else if (jtr.Depth == 2 && jtr.Value != null)
                {
                    switch (jtr.TokenType)
                    {
                        case JsonToken.PropertyName:
                            key = jtr.Value.ToString();
                            break;
                        case JsonToken.String:
                            {
                                value = jtr.Value.ToString();
                                var entry = new TrLangEntry(key, value, langGroup);
                                langGroup.AddEntry(entry);

                                break;
                            }

                        default:
                            throw new Exception(string.Format("TokenType Error: {0}", jtr.TokenType));
                        case JsonToken.Comment:
                            break;
                    }
                }
                else if (jtr.Depth > 2 && jtr.Value != null)
                {
                    throw new Exception("Logical error!");
                }
            }
        }
    }
}

namespace TrResouceEditLib.ResourceIO
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using Mono.Cecil;

    public class TrResourceUtils
    {
        public static void ReplaceResourceFromResourceList(string inputPath, string outputPath, Dictionary<string, byte[]> resourceBuffList)
        {
            bool isFound = false;
            var defaultAssemblyResolver = new DefaultAssemblyResolver();
            defaultAssemblyResolver.AddSearchDirectory(".\\dll\\");
            using (var assemblyDefinition =
                AssemblyDefinition.ReadAssembly(
                    inputPath,
                    new ReaderParameters { AssemblyResolver = defaultAssemblyResolver }))
            {
                foreach (var resourceBuff in resourceBuffList)
                {
                    foreach (var resource in assemblyDefinition.MainModule.Resources)
                    {
                        if (resource.Name.ToLower() == resourceBuff.Key.ToLower())
                        {
                            assemblyDefinition.MainModule.Resources.Remove(resource);
                            isFound = true;
                            break;
                        }
                    }

                    if (isFound)
                    {
                        var item = new EmbeddedResource(resourceBuff.Key, ManifestResourceAttributes.Public, resourceBuff.Value);
                        assemblyDefinition.MainModule.Resources.Add(item);
                    }
                }

                assemblyDefinition.Write(outputPath);
            }
        }

        public static void ReplaceResource(string inputPath, string outputPath, string resourceName, byte[] resourceBuff)
        {
            bool isFound = false;
            using (var assemblyDefinition = AssemblyDefinition.ReadAssembly(inputPath))
            {
                foreach (var resource in assemblyDefinition.MainModule.Resources)
                {
                    if (resource.Name.ToLower() == resourceName.ToLower())
                    {
                        assemblyDefinition.MainModule.Resources.Remove(resource);
                        isFound = true;
                        break;
                    }
                }

                if (isFound)
                {
                    var item = new EmbeddedResource(resourceName, ManifestResourceAttributes.Public, resourceBuff);
                    assemblyDefinition.MainModule.Resources.Add(item);
                    assemblyDefinition.Write(outputPath);
                }
            }
        }

        [Obsolete("このメソッドはまだ未検証です。")]
        public static void AddResource(string inputPath, string outputPath, string resourceName, byte[] resourceBuff)
        {
            using (var assemblyDefinition = AssemblyDefinition.ReadAssembly(inputPath))
            {
                var item = new EmbeddedResource(resourceName, ManifestResourceAttributes.Public, resourceBuff);
                assemblyDefinition.MainModule.Resources.Add(item);
                assemblyDefinition.Write(outputPath);
            }
        }

        public static MemoryStream GetResource(string inputPath, string resourceName)
        {
            using (var asm = AssemblyDefinition.ReadAssembly(inputPath))
            {
                foreach (var resource in asm.MainModule.Resources)
                {
                    if (resource.Name == resourceName)
                    {
                        Stream resourceStream = ((EmbeddedResource)resource).GetResourceStream();
                        byte[] array = new byte[resourceStream.Length];
                        resourceStream.Read(array, 0, array.Length);
                        var memoryStream = new MemoryStream();
                        memoryStream.Write(array, 0, array.Length);
                        memoryStream.Position = 0L;
                        return memoryStream;
                    }
                }
            }

            return null;
        }

        public static bool HasClass(string path, string className)
        {
            using (var asm = AssemblyDefinition.ReadAssembly(path))
            {
                if (asm.MainModule.GetType(className) == null)
                {
                    return false;
                }
            }

            return true;
        }

        public static byte[] StringToArray(string text, Encoding enc = null)
        {
            var encoding = new UTF8Encoding(false);
            enc = enc ?? encoding;
            return enc.GetBytes(text);
        }

        public static string ArrayToString(byte[] buff, Encoding enc = null)
        {
            var encoding = new UTF8Encoding(false);
            enc = enc ?? encoding;
            return enc.GetString(buff);
        }

        public static string StreamToString(MemoryStream ms, Encoding enc = null)
        {
            if (ms == null)
            {
                return string.Empty;
            }

            return TrResourceUtils.ArrayToString(ms.ToArray(), enc);
        }
    }
}

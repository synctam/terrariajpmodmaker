namespace TrResouceEditLib.LaaPatch
{
    using System;
    using System.IO;

    public class LaaPatch
    {
        private const int LaaFlagOffset = 150;
        private string exePath = string.Empty;

        public LaaPatch(string path)
        {
            this.exePath = path;
        }

        private enum Characteristics : ushort
        {
            IMAGE_FILE_RELOCS_STRIPPED = 1,
            IMAGE_FILE_EXECUTABLE_IMAGE,
            IMAGE_FILE_LINE_NUMS_STRIPPED = 4,
            IMAGE_FILE_LOCAL_SYMS_STRIPPED = 8,
            IMAGE_FILE_AGGRESIVE_WS_TRIM = 0x10,
            IMAGE_FILE_LARGE_ADDRESS_AWARE = 0x20,
            IMAGE_FILE_BYTES_REVERSED_LO = 0x80,
            IMAGE_FILE_32BIT_MACHINE = 0x100,
            IMAGE_FILE_DEBUG_STRIPPED = 0x200,
            IMAGE_FILE_REMOVABLE_RUN_FROM_SWAP = 0x400,
            IMAGE_FILE_NET_RUN_FROM_SWAP = 0x800,
            IMAGE_FILE_SYSTEM = 0x1000,
            IMAGE_FILE_DLL = 0x2000,
            IMAGE_FILE_UP_SYSTEM_ONLY = 0x4000,
            IMAGE_FILE_BYTES_REVERSED_HI = 0x8000
        }

        public bool IsLaa()
        {
            using (StreamReader streamReader = new StreamReader(this.exePath))
            {
                using (BinaryReader binaryReader = new BinaryReader(streamReader.BaseStream))
                {
                    binaryReader.BaseStream.Seek(150L, SeekOrigin.Begin);
                    if ((binaryReader.ReadUInt16() & 0x20) == 32)
                    {
                        return true;
                    }

                    return false;
                }
            }
        }

        public bool SetLaa()
        {
            using (FileStream fileStream = new FileStream(this.exePath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
            {
                fileStream.Seek(150L, SeekOrigin.Begin);
                byte[] array = new byte[2];
                fileStream.Read(array, 0, 2);
                Characteristics characteristics = (Characteristics)BitConverter.ToUInt16(array, 0);
                if ((characteristics & Characteristics.IMAGE_FILE_LARGE_ADDRESS_AWARE) == Characteristics.IMAGE_FILE_LARGE_ADDRESS_AWARE)
                {
                    return false;
                }

                characteristics |= Characteristics.IMAGE_FILE_LARGE_ADDRESS_AWARE;
                fileStream.Seek(150L, SeekOrigin.Begin);
                byte[] bytes = BitConverter.GetBytes((ushort)characteristics);
                fileStream.Write(bytes, 0, 2);

                return true;
            }
        }
    }
}
